﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KiNO2.Logic;

namespace KiNO2
{
    public partial class frmAžuriranjeKorisnika : Form
    {
        private Zaposlenik zaposlenik;

        public frmAžuriranjeKorisnika(Zaposlenik odabraniZaposlenik)
        {
            InitializeComponent();

            zaposlenik = odabraniZaposlenik;
        }

        private void frmAžuriranjeKorisnika_Load(object sender, EventArgs e)
        {
            if (zaposlenik != null)
            {
                txtID.Text = zaposlenik.zaposlenikId.ToString();
                txtIme.Text = zaposlenik.ime;
                txtPrezime.Text = zaposlenik.prezime;
                txtEmail.Text = zaposlenik.email;
                txtLozinka.Text = zaposlenik.lozinka;
                txtKontakt.Text = zaposlenik.telefon;
                txtKategorija.Text = zaposlenik.kategorijaZaposlenikaId.ToString();
            }
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }       

        private void btnSpremi_Click(object sender, EventArgs e)
        {
            zaposlenik.ime = txtIme.Text;
            zaposlenik.prezime = txtPrezime.Text;
            zaposlenik.email = txtEmail.Text;
            zaposlenik.lozinka = txtLozinka.Text;
            zaposlenik.telefon = txtKontakt.Text;
            zaposlenik.kategorijaZaposlenikaId = int.Parse(txtKategorija.Text);

            OsobljeLogic.AzuriranjePostojecegKorisnika(zaposlenik);

            Close();
        }

        private void frmAžuriranjeKorisnika_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F1)
            {
                System.Diagnostics.Process.Start(@"help.pdf");
            }
        }
    }
}
