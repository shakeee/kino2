﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KiNO2.Logic;
using KiNODLL;

namespace KiNO2
{
    public partial class frmDodavanjeKorisnika : Form
    {
        public frmDodavanjeKorisnika()
        {
            InitializeComponent();
        }

        private void btnOdustano_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSpremi_Click(object sender, EventArgs e)
        {
            string lozinka = Crypto.sha256(txtLozinka.Text);
            OsobljeLogic.UnosNovogKorisnika(txtIme.Text, txtPrezime.Text, txtEmail.Text, lozinka, txtKontakt.Text, int.Parse(txtKategorija.Text));

            Close();
        }

        private void frmDodavanjeKorisnika_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F1)
            {
                System.Diagnostics.Process.Start(@"help.pdf");
            }
        }
    }
}
