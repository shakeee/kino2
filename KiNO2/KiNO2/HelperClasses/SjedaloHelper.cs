﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KiNO2.HelperClasses
{
    class SjedaloHelper
    {
        public int seatId;
        public int seatTypeId { get; set; }
        public bool seatPositionSet { get; set; }
        public bool seatReserved { get; set; }
        public PictureBox image { get; set; }
        public Point position { get; set; }
    }
}
