﻿namespace KiNO2
{
    partial class frmKarta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.rptKarta = new Microsoft.Reporting.WinForms.ReportViewer();
            this._16058_DBDataSet = new KiNO2._16058_DBDataSet();
            this.tblKartaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tblKartaTableAdapter = new KiNO2._16058_DBDataSetTableAdapters.tblKartaTableAdapter();
            this.RezervacijaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._16058_DBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblKartaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RezervacijaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rptKarta
            // 
            reportDataSource1.Name = "dsKarta";
            reportDataSource1.Value = this.tblKartaBindingSource;
            this.rptKarta.LocalReport.DataSources.Add(reportDataSource1);
            this.rptKarta.LocalReport.ReportEmbeddedResource = "KiNO2.Report1.rdlc";
            this.rptKarta.Location = new System.Drawing.Point(0, 0);
            this.rptKarta.Name = "rptKarta";
            this.rptKarta.Size = new System.Drawing.Size(754, 294);
            this.rptKarta.TabIndex = 0;
            // 
            // _16058_DBDataSet
            // 
            this._16058_DBDataSet.DataSetName = "_16058_DBDataSet";
            this._16058_DBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tblKartaBindingSource
            // 
            this.tblKartaBindingSource.DataMember = "tblKarta";
            this.tblKartaBindingSource.DataSource = this._16058_DBDataSet;
            // 
            // tblKartaTableAdapter
            // 
            this.tblKartaTableAdapter.ClearBeforeFill = true;
            // 
            // RezervacijaBindingSource
            // 
            this.RezervacijaBindingSource.DataSource = typeof(KiNO2.Rezervacija);
            // 
            // frmKarta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 295);
            this.Controls.Add(this.rptKarta);
            this.Name = "frmKarta";
            this.Text = "frmKarta";
            this.Load += new System.EventHandler(this.frmKarta_Load);
            ((System.ComponentModel.ISupportInitialize)(this._16058_DBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblKartaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RezervacijaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource RezervacijaBindingSource;
        private Microsoft.Reporting.WinForms.ReportViewer rptKarta;
        private System.Windows.Forms.BindingSource tblKartaBindingSource;
        private _16058_DBDataSet _16058_DBDataSet;
        private _16058_DBDataSetTableAdapters.tblKartaTableAdapter tblKartaTableAdapter;
    }
}