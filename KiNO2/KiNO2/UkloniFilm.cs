﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KiNO2.Logic;

namespace KiNO2
{
    public partial class UkloniFilm : Form
    {
        public UkloniFilm()
        {
            InitializeComponent();
        }

        private void UkloniFilm_Load(object sender, EventArgs e)
        {
            BindingList<Film> flm = FilmoviLogic.PrikaziFIlmove();
            filmBindingSource.DataSource = flm;
        }

        private void btnUkloni_Click(object sender, EventArgs e)
        {
            Film selFilm = filmBindingSource.Current as Film;
            FilmoviLogic.UkloniFilmove(selFilm);

            BindingList<Film> flm = FilmoviLogic.PrikaziFIlmove();
            filmBindingSource.DataSource = flm;
        }

        private void UkloniFilm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F1)
            {
                System.Diagnostics.Process.Start(@"help.pdf");
            }
        }
    }
}
