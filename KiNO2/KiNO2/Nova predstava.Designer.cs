﻿namespace KiNO2
{
    partial class Nova_predstava
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmBoxTipPredstave = new System.Windows.Forms.ComboBox();
            this.tipPredstaveBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnNoPredSpremi = new System.Windows.Forms.Button();
            this.DatumVrijemeOdaberi = new System.Windows.Forms.DateTimePicker();
            this.lblZaposlenik = new System.Windows.Forms.Label();
            this.lblTipPredstave = new System.Windows.Forms.Label();
            this.lblDvorane = new System.Windows.Forms.Label();
            this.lblDatumVrijeme = new System.Windows.Forms.Label();
            this.lblFilmovi = new System.Windows.Forms.Label();
            this.cmBoxZaposlenik = new System.Windows.Forms.ComboBox();
            this.zaposlenikBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmBoxDvorane = new System.Windows.Forms.ComboBox();
            this.dvoranaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmBoxFilmovi = new System.Windows.Forms.ComboBox();
            this.filmBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            ((System.ComponentModel.ISupportInitialize)(this.tipPredstaveBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zaposlenikBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvoranaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filmBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // cmBoxTipPredstave
            // 
            this.cmBoxTipPredstave.DataSource = this.tipPredstaveBindingSource;
            this.cmBoxTipPredstave.DisplayMember = "naziv";
            this.cmBoxTipPredstave.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmBoxTipPredstave.FormattingEnabled = true;
            this.cmBoxTipPredstave.Location = new System.Drawing.Point(12, 214);
            this.cmBoxTipPredstave.Name = "cmBoxTipPredstave";
            this.cmBoxTipPredstave.Size = new System.Drawing.Size(121, 21);
            this.cmBoxTipPredstave.TabIndex = 15;
            this.cmBoxTipPredstave.ValueMember = "tipPredstaveId";
            // 
            // tipPredstaveBindingSource
            // 
            this.tipPredstaveBindingSource.DataSource = typeof(KiNO2.TipPredstave);
            // 
            // btnNoPredSpremi
            // 
            this.btnNoPredSpremi.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnNoPredSpremi.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNoPredSpremi.FlatAppearance.BorderSize = 0;
            this.btnNoPredSpremi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.btnNoPredSpremi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNoPredSpremi.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnNoPredSpremi.Location = new System.Drawing.Point(180, 198);
            this.btnNoPredSpremi.Name = "btnNoPredSpremi";
            this.btnNoPredSpremi.Size = new System.Drawing.Size(147, 37);
            this.btnNoPredSpremi.TabIndex = 14;
            this.btnNoPredSpremi.Text = "Spremi";
            this.btnNoPredSpremi.UseVisualStyleBackColor = false;
            this.btnNoPredSpremi.Click += new System.EventHandler(this.btnNoPredSpremi_Click);
            // 
            // DatumVrijemeOdaberi
            // 
            this.DatumVrijemeOdaberi.CustomFormat = "dd MM yyyy hh mm ss";
            this.DatumVrijemeOdaberi.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DatumVrijemeOdaberi.Location = new System.Drawing.Point(15, 126);
            this.DatumVrijemeOdaberi.Name = "DatumVrijemeOdaberi";
            this.DatumVrijemeOdaberi.Size = new System.Drawing.Size(143, 20);
            this.DatumVrijemeOdaberi.TabIndex = 13;
            this.DatumVrijemeOdaberi.Value = new System.DateTime(2016, 6, 19, 0, 0, 0, 0);
            // 
            // lblZaposlenik
            // 
            this.lblZaposlenik.AutoSize = true;
            this.lblZaposlenik.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblZaposlenik.Location = new System.Drawing.Point(177, 16);
            this.lblZaposlenik.Name = "lblZaposlenik";
            this.lblZaposlenik.Size = new System.Drawing.Size(73, 17);
            this.lblZaposlenik.TabIndex = 8;
            this.lblZaposlenik.Text = "Zaposlenik:";
            // 
            // lblTipPredstave
            // 
            this.lblTipPredstave.AutoSize = true;
            this.lblTipPredstave.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTipPredstave.Location = new System.Drawing.Point(9, 184);
            this.lblTipPredstave.Name = "lblTipPredstave";
            this.lblTipPredstave.Size = new System.Drawing.Size(91, 17);
            this.lblTipPredstave.TabIndex = 9;
            this.lblTipPredstave.Text = "Tip predstave:";
            // 
            // lblDvorane
            // 
            this.lblDvorane.AutoSize = true;
            this.lblDvorane.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDvorane.Location = new System.Drawing.Point(177, 97);
            this.lblDvorane.Name = "lblDvorane";
            this.lblDvorane.Size = new System.Drawing.Size(60, 17);
            this.lblDvorane.TabIndex = 10;
            this.lblDvorane.Text = "Dvorane:";
            // 
            // lblDatumVrijeme
            // 
            this.lblDatumVrijeme.AutoSize = true;
            this.lblDatumVrijeme.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDatumVrijeme.Location = new System.Drawing.Point(12, 97);
            this.lblDatumVrijeme.Name = "lblDatumVrijeme";
            this.lblDatumVrijeme.Size = new System.Drawing.Size(102, 17);
            this.lblDatumVrijeme.TabIndex = 11;
            this.lblDatumVrijeme.Text = "Datum i vrijeme:";
            // 
            // lblFilmovi
            // 
            this.lblFilmovi.AutoSize = true;
            this.lblFilmovi.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblFilmovi.Location = new System.Drawing.Point(12, 16);
            this.lblFilmovi.Name = "lblFilmovi";
            this.lblFilmovi.Size = new System.Drawing.Size(51, 17);
            this.lblFilmovi.TabIndex = 12;
            this.lblFilmovi.Text = "Filmovi:";
            // 
            // cmBoxZaposlenik
            // 
            this.cmBoxZaposlenik.DataSource = this.zaposlenikBindingSource;
            this.cmBoxZaposlenik.DisplayMember = "ime";
            this.cmBoxZaposlenik.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmBoxZaposlenik.FormattingEnabled = true;
            this.cmBoxZaposlenik.Location = new System.Drawing.Point(180, 41);
            this.cmBoxZaposlenik.Name = "cmBoxZaposlenik";
            this.cmBoxZaposlenik.Size = new System.Drawing.Size(121, 21);
            this.cmBoxZaposlenik.TabIndex = 5;
            this.cmBoxZaposlenik.ValueMember = "zaposlenikId";
            // 
            // zaposlenikBindingSource
            // 
            this.zaposlenikBindingSource.DataSource = typeof(KiNO2.Zaposlenik);
            // 
            // cmBoxDvorane
            // 
            this.cmBoxDvorane.DataSource = this.dvoranaBindingSource;
            this.cmBoxDvorane.DisplayMember = "naziv";
            this.cmBoxDvorane.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmBoxDvorane.FormattingEnabled = true;
            this.cmBoxDvorane.Location = new System.Drawing.Point(177, 126);
            this.cmBoxDvorane.Name = "cmBoxDvorane";
            this.cmBoxDvorane.Size = new System.Drawing.Size(121, 21);
            this.cmBoxDvorane.TabIndex = 6;
            this.cmBoxDvorane.ValueMember = "dvoranaId";
            // 
            // dvoranaBindingSource
            // 
            this.dvoranaBindingSource.DataSource = typeof(KiNO2.Dvorana);
            // 
            // cmBoxFilmovi
            // 
            this.cmBoxFilmovi.DataSource = this.filmBindingSource;
            this.cmBoxFilmovi.DisplayMember = "naziv";
            this.cmBoxFilmovi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmBoxFilmovi.FormattingEnabled = true;
            this.cmBoxFilmovi.Location = new System.Drawing.Point(12, 41);
            this.cmBoxFilmovi.Name = "cmBoxFilmovi";
            this.cmBoxFilmovi.Size = new System.Drawing.Size(121, 21);
            this.cmBoxFilmovi.TabIndex = 7;
            this.cmBoxFilmovi.ValueMember = "filmId";
            // 
            // filmBindingSource
            // 
            this.filmBindingSource.DataSource = typeof(KiNO2.Film);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\help.pdf";
            // 
            // Nova_predstava
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(362, 272);
            this.Controls.Add(this.cmBoxTipPredstave);
            this.Controls.Add(this.btnNoPredSpremi);
            this.Controls.Add(this.DatumVrijemeOdaberi);
            this.Controls.Add(this.lblZaposlenik);
            this.Controls.Add(this.lblTipPredstave);
            this.Controls.Add(this.lblDvorane);
            this.Controls.Add(this.lblDatumVrijeme);
            this.Controls.Add(this.lblFilmovi);
            this.Controls.Add(this.cmBoxZaposlenik);
            this.Controls.Add(this.cmBoxDvorane);
            this.Controls.Add(this.cmBoxFilmovi);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "C:\\help.pdf");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.KeywordIndex);
            this.KeyPreview = true;
            this.Name = "Nova_predstava";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Nova Predstava";
            this.Load += new System.EventHandler(this.Nova_predstava_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Nova_predstava_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.tipPredstaveBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zaposlenikBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvoranaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filmBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmBoxTipPredstave;
        private System.Windows.Forms.Button btnNoPredSpremi;
        private System.Windows.Forms.DateTimePicker DatumVrijemeOdaberi;
        private System.Windows.Forms.Label lblZaposlenik;
        private System.Windows.Forms.Label lblTipPredstave;
        private System.Windows.Forms.Label lblDvorane;
        private System.Windows.Forms.Label lblDatumVrijeme;
        private System.Windows.Forms.Label lblFilmovi;
        private System.Windows.Forms.ComboBox cmBoxZaposlenik;
        private System.Windows.Forms.ComboBox cmBoxDvorane;
        private System.Windows.Forms.ComboBox cmBoxFilmovi;
        private System.Windows.Forms.BindingSource tipPredstaveBindingSource;
        private System.Windows.Forms.BindingSource zaposlenikBindingSource;
        private System.Windows.Forms.BindingSource dvoranaBindingSource;
        private System.Windows.Forms.BindingSource filmBindingSource;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}