﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TMDbLib;
using TMDbLib.Client;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Movies;
using RestSharp;
using TMDbLib.Objects.Search;
using TMDbLib.Utilities;
using TMDbLib.Objects.Genres;
using KiNO2.Logic;

namespace KiNO2
{
    public partial class UnosFilma : Form
    {
        public UnosFilma()
        {
            InitializeComponent();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            List<Film> film = FilmoviLogic.traziFIlm(txtBoxTrazenje.Text);
            foreach (Film item in film)
            {
                listBox1.Items.Add(item);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            TMDbClient client = new TMDbClient("6af5b16ff389e8c39110a04976170097");
            ImagesWithId images = client.GetMovieImages(((Film)listBox1.SelectedItem).tmdbID);
            Movie movie = client.GetMovie(((Film)listBox1.SelectedItem).tmdbID);

            client.GetConfig();

            try
            {
                pictureBox1.Load(((Film)listBox1.SelectedItem).slikaUri);
                Naslov.Text = movie.Title;
                Zanr.Text = (movie.Genres[0].Name).ToString();
                Opis.Text = movie.Overview;
            }
            catch (Exception)
            {
                MessageBox.Show("Ne postoje podaci za trazeni film!", "Upozorenje", MessageBoxButtons.OK);
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            FilmoviLogic.UnosFilma(listBox1.SelectedItem);
        }

        private void UnosFilma_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F1)
            {
                System.Diagnostics.Process.Start(@"help.pdf");
            }
        }
    }
}
