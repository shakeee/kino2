﻿namespace KiNO2
{
    partial class frmDvoraneForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNumberOfRows = new System.Windows.Forms.Label();
            this.lblNumberOfColumns = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxNameOfTheatherRoom = new System.Windows.Forms.TextBox();
            this.nudNumberOfRows = new System.Windows.Forms.NumericUpDown();
            this.nudNumberOfColumns = new System.Windows.Forms.NumericUpDown();
            this.gbxAvailableSeats = new System.Windows.Forms.GroupBox();
            this.pbxLoversSeat = new System.Windows.Forms.PictureBox();
            this.pbxSingleSeat = new System.Windows.Forms.PictureBox();
            this.btnSaveTheatherRoom = new System.Windows.Forms.Button();
            this.gbxActions = new System.Windows.Forms.GroupBox();
            this.pbxDeleteSeat = new System.Windows.Forms.PictureBox();
            this.pnlTheaterRoom = new System.Windows.Forms.Panel();
            this.pnlTheaterRoomEditor = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfRows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfColumns)).BeginInit();
            this.gbxAvailableSeats.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLoversSeat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSingleSeat)).BeginInit();
            this.gbxActions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDeleteSeat)).BeginInit();
            this.pnlTheaterRoom.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNumberOfRows
            // 
            this.lblNumberOfRows.AutoSize = true;
            this.lblNumberOfRows.Location = new System.Drawing.Point(13, 47);
            this.lblNumberOfRows.Name = "lblNumberOfRows";
            this.lblNumberOfRows.Size = new System.Drawing.Size(64, 13);
            this.lblNumberOfRows.TabIndex = 1;
            this.lblNumberOfRows.Text = "Broj redova:";
            // 
            // lblNumberOfColumns
            // 
            this.lblNumberOfColumns.AutoSize = true;
            this.lblNumberOfColumns.Location = new System.Drawing.Point(202, 47);
            this.lblNumberOfColumns.Name = "lblNumberOfColumns";
            this.lblNumberOfColumns.Size = new System.Drawing.Size(69, 13);
            this.lblNumberOfColumns.TabIndex = 2;
            this.lblNumberOfColumns.Text = "Broj stupaca:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Broj stupaca:";
            // 
            // tbxNameOfTheatherRoom
            // 
            this.tbxNameOfTheatherRoom.Location = new System.Drawing.Point(89, 14);
            this.tbxNameOfTheatherRoom.Name = "tbxNameOfTheatherRoom";
            this.tbxNameOfTheatherRoom.Size = new System.Drawing.Size(265, 20);
            this.tbxNameOfTheatherRoom.TabIndex = 4;
            this.tbxNameOfTheatherRoom.Text = "DV1";
            // 
            // nudNumberOfRows
            // 
            this.nudNumberOfRows.Location = new System.Drawing.Point(89, 45);
            this.nudNumberOfRows.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudNumberOfRows.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudNumberOfRows.Name = "nudNumberOfRows";
            this.nudNumberOfRows.Size = new System.Drawing.Size(68, 20);
            this.nudNumberOfRows.TabIndex = 5;
            this.nudNumberOfRows.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudNumberOfRows.ValueChanged += new System.EventHandler(this.nudNumberOfRows_ValueChanged);
            // 
            // nudNumberOfColumns
            // 
            this.nudNumberOfColumns.Location = new System.Drawing.Point(286, 45);
            this.nudNumberOfColumns.Maximum = new decimal(new int[] {
            22,
            0,
            0,
            0});
            this.nudNumberOfColumns.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudNumberOfColumns.Name = "nudNumberOfColumns";
            this.nudNumberOfColumns.Size = new System.Drawing.Size(68, 20);
            this.nudNumberOfColumns.TabIndex = 6;
            this.nudNumberOfColumns.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudNumberOfColumns.ValueChanged += new System.EventHandler(this.nudNumberOfColumns_ValueChanged);
            // 
            // gbxAvailableSeats
            // 
            this.gbxAvailableSeats.Controls.Add(this.pbxLoversSeat);
            this.gbxAvailableSeats.Controls.Add(this.pbxSingleSeat);
            this.gbxAvailableSeats.Location = new System.Drawing.Point(16, 81);
            this.gbxAvailableSeats.Name = "gbxAvailableSeats";
            this.gbxAvailableSeats.Size = new System.Drawing.Size(255, 61);
            this.gbxAvailableSeats.TabIndex = 8;
            this.gbxAvailableSeats.TabStop = false;
            this.gbxAvailableSeats.Text = "Dostupna sjedala";
            // 
            // pbxLoversSeat
            // 
            this.pbxLoversSeat.BackColor = System.Drawing.Color.Transparent;
            this.pbxLoversSeat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxLoversSeat.Image = global::KiNO2.Properties.Resources.l;
            this.pbxLoversSeat.Location = new System.Drawing.Point(41, 25);
            this.pbxLoversSeat.Name = "pbxLoversSeat";
            this.pbxLoversSeat.Size = new System.Drawing.Size(40, 20);
            this.pbxLoversSeat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxLoversSeat.TabIndex = 6;
            this.pbxLoversSeat.TabStop = false;
            this.pbxLoversSeat.Click += new System.EventHandler(this.pbxLoversSeat_Click);
            // 
            // pbxSingleSeat
            // 
            this.pbxSingleSeat.BackColor = System.Drawing.Color.Transparent;
            this.pbxSingleSeat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxSingleSeat.Image = global::KiNO2.Properties.Resources.s;
            this.pbxSingleSeat.Location = new System.Drawing.Point(15, 25);
            this.pbxSingleSeat.Name = "pbxSingleSeat";
            this.pbxSingleSeat.Size = new System.Drawing.Size(20, 20);
            this.pbxSingleSeat.TabIndex = 5;
            this.pbxSingleSeat.TabStop = false;
            this.pbxSingleSeat.Click += new System.EventHandler(this.pbxSingleSeat_Click);
            // 
            // btnSaveTheatherRoom
            // 
            this.btnSaveTheatherRoom.Location = new System.Drawing.Point(393, 629);
            this.btnSaveTheatherRoom.Name = "btnSaveTheatherRoom";
            this.btnSaveTheatherRoom.Size = new System.Drawing.Size(75, 23);
            this.btnSaveTheatherRoom.TabIndex = 9;
            this.btnSaveTheatherRoom.Text = "Spremi";
            this.btnSaveTheatherRoom.UseVisualStyleBackColor = true;
            this.btnSaveTheatherRoom.Click += new System.EventHandler(this.btnSaveTheatherRoom_Click);
            // 
            // gbxActions
            // 
            this.gbxActions.Controls.Add(this.pbxDeleteSeat);
            this.gbxActions.Location = new System.Drawing.Point(295, 81);
            this.gbxActions.Name = "gbxActions";
            this.gbxActions.Size = new System.Drawing.Size(59, 61);
            this.gbxActions.TabIndex = 10;
            this.gbxActions.TabStop = false;
            this.gbxActions.Text = "Akcije";
            // 
            // pbxDeleteSeat
            // 
            this.pbxDeleteSeat.BackColor = System.Drawing.Color.Transparent;
            this.pbxDeleteSeat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxDeleteSeat.Image = global::KiNO2.Properties.Resources.delete;
            this.pbxDeleteSeat.Location = new System.Drawing.Point(15, 25);
            this.pbxDeleteSeat.Name = "pbxDeleteSeat";
            this.pbxDeleteSeat.Size = new System.Drawing.Size(20, 20);
            this.pbxDeleteSeat.TabIndex = 6;
            this.pbxDeleteSeat.TabStop = false;
            this.pbxDeleteSeat.Click += new System.EventHandler(this.pbxDeleteSeat_Click);
            // 
            // pnlTheaterRoom
            // 
            this.pnlTheaterRoom.BackColor = System.Drawing.Color.IndianRed;
            this.pnlTheaterRoom.BackgroundImage = global::KiNO2.Properties.Resources.screen;
            this.pnlTheaterRoom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlTheaterRoom.Controls.Add(this.pnlTheaterRoomEditor);
            this.pnlTheaterRoom.Location = new System.Drawing.Point(16, 159);
            this.pnlTheaterRoom.Name = "pnlTheaterRoom";
            this.pnlTheaterRoom.Size = new System.Drawing.Size(452, 457);
            this.pnlTheaterRoom.TabIndex = 7;
            // 
            // pnlTheaterRoomEditor
            // 
            this.pnlTheaterRoomEditor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlTheaterRoomEditor.BackColor = System.Drawing.Color.Transparent;
            this.pnlTheaterRoomEditor.Location = new System.Drawing.Point(138, 160);
            this.pnlTheaterRoomEditor.Name = "pnlTheaterRoomEditor";
            this.pnlTheaterRoomEditor.Size = new System.Drawing.Size(172, 159);
            this.pnlTheaterRoomEditor.TabIndex = 0;
            this.pnlTheaterRoomEditor.MouseEnter += new System.EventHandler(this.pnlTheaterRoomEditor_MouseEnter);
            this.pnlTheaterRoomEditor.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlTheaterRoomEditor_MouseMove);
            // 
            // frmDvoraneForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 664);
            this.Controls.Add(this.gbxActions);
            this.Controls.Add(this.btnSaveTheatherRoom);
            this.Controls.Add(this.gbxAvailableSeats);
            this.Controls.Add(this.pnlTheaterRoom);
            this.Controls.Add(this.nudNumberOfColumns);
            this.Controls.Add(this.nudNumberOfRows);
            this.Controls.Add(this.tbxNameOfTheatherRoom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNumberOfColumns);
            this.Controls.Add(this.lblNumberOfRows);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "frmDvoraneForma";
            this.Text = "frmDvoraneForma";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDvoraneForma_FormClosing);
            this.Load += new System.EventHandler(this.frmDvoraneForma_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmDvoraneForma_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfRows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfColumns)).EndInit();
            this.gbxAvailableSeats.ResumeLayout(false);
            this.gbxAvailableSeats.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLoversSeat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSingleSeat)).EndInit();
            this.gbxActions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxDeleteSeat)).EndInit();
            this.pnlTheaterRoom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTheaterRoomEditor;
        private System.Windows.Forms.Label lblNumberOfRows;
        private System.Windows.Forms.Label lblNumberOfColumns;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxNameOfTheatherRoom;
        private System.Windows.Forms.NumericUpDown nudNumberOfRows;
        private System.Windows.Forms.NumericUpDown nudNumberOfColumns;
        private System.Windows.Forms.Panel pnlTheaterRoom;
        private System.Windows.Forms.GroupBox gbxAvailableSeats;
        private System.Windows.Forms.PictureBox pbxLoversSeat;
        private System.Windows.Forms.PictureBox pbxSingleSeat;
        private System.Windows.Forms.Button btnSaveTheatherRoom;
        private System.Windows.Forms.GroupBox gbxActions;
        private System.Windows.Forms.PictureBox pbxDeleteSeat;
    }
}