namespace KiNO2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RezerviranoSjedalo")]
    public partial class RezerviranoSjedalo
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int rezerviranoSjedaloId { get; set; }

        /// <summary>
        /// Foreign Key, references sjedalo
        /// </summary>
        [Required]
        public int sjedaloId { get; set; }

        /// <summary>
        /// Foreign Key, references Rezervacija
        /// </summary>
        [Required]
        public int rezervacijaId { get; set; }

        /// <summary>
        /// Seat references only one element (which has to be seat) in hall
        /// </summary>
        [ForeignKey("sjedaloId")]
        public virtual Sjedalo Sjedalo { get; set; }

        /// <summary>
        /// One seat is reserved by only one reservation (per show)
        /// </summary>
        [ForeignKey("rezervacijaId")]
        public virtual Rezervacija Rezervacija { get; set; }
    }
}
