namespace KiNO2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipSjedala")]
    public partial class TipSjedala
    {
        public TipSjedala()
        {
            Sjedala = new HashSet<Sjedalo>();
        }

        /// <summary>
        /// Primary Key
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int tipSjedalaId { get; set; }

        /// <summary>
        /// Relative size of the seat in units of 'one seat size', e.g. 'Ljubavno sjedalo has 2 seats width length, thus size is 2'
        /// </summary>
        [Required]
        public int velicina { get; set; }

        /// <summary>
        /// Name of the element, e.g. 'Sjedalo', 'Ljubavno sjedalo', 'VIP' ...
        /// </summary>
        [Required]
        [StringLength(15)]
        public string naziv { get; set; }

        /// <summary>
        /// Multiple seats may be of same type
        /// </summary>
        public virtual ICollection<Sjedalo> Sjedala { get; set; }
    }
}
