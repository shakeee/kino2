namespace KiNO2
{
    using System;
    using System.Drawing;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Film")]
    public partial class Film
    {
        public Film()
        {
            Predstave = new HashSet<Predstava>();
        }

        public override string ToString()
        {
            return this.naziv;
        }

        /// <summary>
        /// Primary Key
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int filmId { get; set; }

        /// <summary>
        /// Foreign Key, references zanr
        /// </summary>
        [Required]
        [StringLength(50)]
        public string zanrId { get; set; }

        /// <summary>
        /// tmdbID
        /// </summary>
        [Required]
        public int tmdbID { get; set; }

        /// <summary>
        /// Movie title
        /// </summary>
        [Required]
        [StringLength(100)]
        public string naziv { get; set; }

        /// <summary>
        /// Movie description
        /// </summary>
        [Required]
        [StringLength(1000)]
        public string opis { get; set; }

        /// <summary>
        /// Movie description
        /// </summary>
        [Required]
        [StringLength(1000)]
        public string slikaUri { get; set; }



        /// <summary>
        /// Each movie may be shown in multiple shows
        /// </summary>
        public virtual ICollection<Predstava> Predstave { get; set; }
    }
}
