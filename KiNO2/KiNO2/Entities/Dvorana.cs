namespace KiNO2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Dvorana")]
    public partial class Dvorana
    {
        public Dvorana()
        {
            Sjedala = new HashSet<Sjedalo>();
            Predstave = new HashSet<Predstava>();
        }

        /// <summary>
        /// Primary key
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int dvoranaId { get; set; }

        /// <summary>
        /// Name of the movie threater hall, e.g. 'DV1' etc.
        /// </summary>
        [Required]
        [StringLength(10)]
        public string naziv { get; set; }

        /// <summary>
        /// Relative width of the hall measured in units of 'one seat width', e.g. 'Dvorana 1 has 20 seats width length'
        /// </summary>
        [Required]
        public int sirina { get; set; }

        /// <summary>
        /// Relative height of the hall measured in units of 'one seat height', e.g. 'Dvorana 3 has 18 seats height length'
        /// </summary>
        [Required]
        public int duljina { get; set; }

        /// <summary>
        /// Each threater hall consists of multiple seats such as seats and double seats, etc.
        /// </summary>
        public virtual ICollection<Sjedalo> Sjedala { get; set; }

        /// <summary>
        /// In each threater hall multiple shows may be held
        /// </summary>
        public virtual ICollection<Predstava> Predstave { get; set; }
    }
}
