namespace KiNO2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("KategorijaZaposlenika")]
    public partial class KategorijaZaposlenika
    {
        public KategorijaZaposlenika()
        {
            Zaposlenici = new HashSet<Zaposlenik>();
        }
        /// <summary>
        /// Primary Key
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int kategorijaZaposlenikaId { get; set; }

        /// <summary>
        /// Category Name
        /// </summary>
        [Required]
        [StringLength(50)]
        public string naziv { get; set; }

        /// <summary>
        /// Category Description
        /// </summary>
        [Required]
        [StringLength(1000)]
        public string opis { get; set; }

        /// <summary>
        /// Employee Category may contain multiple Employees
        /// </summary>
        public virtual ICollection<Zaposlenik> Zaposlenici { get; set; }
    }
}
