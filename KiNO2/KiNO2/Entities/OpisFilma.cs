﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace KiNO2.Entities
{
    class OpisFilma
    {
        public string Slika { get; set; }
        public string Naslov { get; set; }
        public string Opis { get; set; }

        public OpisFilma(string naslov, string opis, string slika)
        {
            this.Naslov = naslov;
            this.Opis = opis;
            this.Slika = slika;
        }
    }
}
