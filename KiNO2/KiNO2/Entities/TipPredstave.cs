﻿namespace KiNO2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipPredstave")]
    public partial class TipPredstave
    {
        public TipPredstave()
        {
            Predstave = new HashSet<Predstava>();
        }

        /// <summary>
        /// Primary Key
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int tipPredstaveId { get; set; }

        /// <summary>
        /// Price per seat
        /// </summary>
        [Required]
        [Column(TypeName = "money")]
        public decimal cijenaPredstave { get; set; }

        /// <summary>
        /// Name of the show type, e.g. '2D', '3D', '4D' ...
        /// </summary>
        [Required]
        [StringLength(15)]
        public string naziv { get; set; }

        /// <summary>
        /// Multiple seats may be of same type
        /// </summary>
        public virtual ICollection<Predstava> Predstave { get; set; }
    }
}
