namespace KiNO2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sjedalo")]
    public partial class Sjedalo
    {
        public Sjedalo()
        {
            RezerviranaSjedala = new HashSet<RezerviranoSjedalo>();
        }

        /// <summary>
        /// Primary Key
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int sjedaloId { get; set; }

        /// <summary>
        /// Foreign key, references Element
        /// </summary>
        [Required]
        public int tipSjedalaId { get; set; }

        /// <summary>
        /// Foreign key, references Dvorana
        /// </summary>
        [Required]
        public int dvoranaId { get; set; }

        /// <summary>
        /// Seat position on x axis
        /// </summary>
        [Required]
        public int pozicijaX { get; set; }

        /// <summary>
        /// Seat position on y axis
        /// </summary>
        [Required]
        public int pozicijaY { get; set; }

        /// <summary>
        /// One seat can physically be in only one hall
        /// </summary>
        [ForeignKey("dvoranaId")]
        public virtual Dvorana Dvorana { get; set; }

        /// <summary>
        /// One seat can be only one seat type
        /// </summary>
        [ForeignKey("tipSjedalaId")]
        public virtual TipSjedala TipSjedala { get; set; }

        /// <summary>
        /// If seat type is 'Sjedalo' or 'Ljubavno sjedalo', then it can be reserved and/or bought per show
        /// </summary>
        public virtual ICollection<RezerviranoSjedalo> RezerviranaSjedala { get; set; }
    }
}
