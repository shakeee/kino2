﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiNO2.Entities
{
    class OpisPredstava
    {
        public int id { get; set; }
        public string Film { get; set; }
        public DateTime Datum { get; set; }
        public TimeSpan Vrijeme { get; set; }
        public string Dvorana { get; set; }

        public OpisPredstava(int id, string film, DateTime datum, TimeSpan vrijeme, string dvorana)
        {
            this.id = id;
            this.Film = film;
            this.Datum = datum;
            this.Vrijeme = vrijeme;
            this.Dvorana = dvorana;
        }
    }
}
