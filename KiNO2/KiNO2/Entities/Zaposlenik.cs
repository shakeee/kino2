namespace KiNO2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Zaposlenik")]
    public partial class Zaposlenik
    {
        public Zaposlenik()
        {
            Predstave = new HashSet<Predstava>();
        }
        
        /// <summary>
        /// Primary Key
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int zaposlenikId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [Required]
        [StringLength(30)]
        public string ime { get; set; }

        /// <summary>
        /// Surename
        /// </summary>
        [Required]
        [StringLength(30)]
        public string prezime { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Required]
        [StringLength(50)]
        public string email { get; set; }

        /// <summary>
        /// Password (SHA-256)
        /// </summary>
        [Required]
        [StringLength(64)]
        public string lozinka { get; set; }

        /// <summary>
        /// Phone number
        /// </summary>
        [Required]
        [StringLength(15)]
        public string telefon { get; set; }

        /// <summary>
        /// Foreign Key, references
        /// </summary>
        [Required]
        public int kategorijaZaposlenikaId { get; set; }

        /// <summary>
        /// Eployee may be in only one category at a time
        /// </summary>
        [ForeignKey("kategorijaZaposlenikaId")]
        public virtual KategorijaZaposlenika KategorijaZaposlenika { get; set; }

        /// <summary>
        /// Employee may be in charge of multiple shows
        /// </summary>
        public virtual ICollection<Predstava> Predstave { get; set; }
    }
}
