namespace KiNO2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data;

    [Table("Predstava")]
    public partial class Predstava
    {
        public Predstava()
        {
            Rezervacije = new HashSet<Rezervacija>();
        }

        /// <summary>
        /// Primary Key
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int predstavaId { get; set; }

        /// <summary>
        /// Foreign Key, references tipPredstave
        /// </summary>
        public int tipPredstaveId { get; set; }

        /// <summary>
        /// Foreign Key, references Dvorana
        /// </summary>
        public int dvoranaId { get; set; }

        /// <summary>
        /// Foreign Key, references Dvorana
        /// </summary>
        public int filmId { get; set; }

        /// <summary>
        /// Date of the show
        /// </summary>
        [Column(TypeName = "date")]
        public DateTime datum { get; set; }

        /// <summary>
        /// Time of the show
        /// </summary>
        public TimeSpan vrijeme { get; set; }

        /// <summary>
        /// Id of employee that is in charge of that particular show
        /// </summary>
        public int zaposlenikId { get; set; }

        /// <summary>
        /// One particular show is of one type only
        /// </summary>
        [ForeignKey("tipPredstaveId")]
        public virtual TipPredstave TipPredstave { get; set; }

        /// <summary>
        /// One particular show is held in one hall
        /// </summary>
        [ForeignKey("dvoranaId")]
        public virtual Dvorana Dvorana { get; set; }

        /// <summary>
        /// One particular show plays only one movie
        /// </summary>
        [ForeignKey("filmId")]
        public virtual Film Film { get; set; }

        /// <summary>
        /// Only one employee is in charge of one show
        /// </summary>
        [ForeignKey("zaposlenikId")]
        public virtual Zaposlenik Zaposlenik { get; set; }

        /// <summary>
        /// There may be multiple reservations for one show
        /// </summary>
        public virtual ICollection<Rezervacija> Rezervacije { get; set; }

        /// <summary>
        /// Retrun full info on show as string - not mapped to the database
        /// </summary>
        [NotMapped]
        public string infoPredstave
        {
            get
            {
                using (var db = new KinoDBContext())
                {
                    db.Predstave.Attach(this);
                    return vrijeme.ToString(@"hh\:mm") + " - " + Dvorana.naziv + " " + TipPredstave.naziv;
                }
            }
        }
    }
}
