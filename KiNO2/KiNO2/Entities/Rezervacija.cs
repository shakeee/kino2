namespace KiNO2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Rezervacija")]
    public partial class Rezervacija
    {
        public Rezervacija()
        {
            RezerviranaSjedala = new HashSet<RezerviranoSjedalo>();
            kupljeno = false;
        }

        /// <summary>
        /// Primary Key
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int rezervacijaId { get; set; }

        /// <summary>
        /// Foreign Key, references Predstava
        /// </summary>
        [Required]
        public int predstavaId { get; set; }

        /// <summary>
        /// Name of the person who reserved seats at the show
        /// </summary>
        [StringLength(30)]
        public string ime { get; set; }

        /// <summary>
        /// Surename of the person who reserved seats at the show
        /// </summary>
        [StringLength(30)]
        public string prezime { get; set; }

        /// <summary>
        /// Phone number of the person who reserved seats at the show
        /// </summary>
        [StringLength(30)]
        public string telefon { get; set; }

        /// <summary>
        /// Price for the tickets
        /// </summary>
        [Required]
        [Column(TypeName = "money")]
        public decimal cijena { get; set; }

        /// <summary>
        /// If reservation is bought then kupljeno is true
        /// </summary>
        [Required]
        public bool kupljeno { get; set; }

        /// <summary>
        /// Reservation applies to only one show
        /// </summary>
        [ForeignKey("predstavaId")]
        public virtual Predstava Predstava { get; set; }

        /// <summary>
        /// Each reservation may have multiple seats reserved
        /// </summary>
        public virtual ICollection<RezerviranoSjedalo> RezerviranaSjedala { get; set; }
       
        /// <summary>
        /// 
        /// </summary>
        [NotMapped]
        public string infoRezervacija
        {
            get
            {
                using (var db = new KinoDBContext())
                {
                    db.Rezervacije.Attach(this);
                    return rezervacijaId.ToString() + " " + ime + " "  + prezime + " " + "NAZIV FILMA: " +Predstava.Film.naziv + " CIJENA: " + Math.Round(cijena,2) + " kn," +  " BROJ REZERVIRANIH SJEDALA:" + RezerviranaSjedala.Count;
                }
            }
        }
    }
}
