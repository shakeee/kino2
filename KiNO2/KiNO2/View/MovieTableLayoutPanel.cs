﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KiNO2.Entities;

namespace KiNO2.View
{
    class MovieTableLayoutPanel:TableLayoutPanel
    {
        public float visinaOmjer { get; set; } = 1.5f;

        public void ShowMovies(List<OpisFilma> opisiFilmova)
        {
            int red, stupac, i;

            this.Controls.Clear();

            //Kreiranje panela po redovima, raspoređivanje panela po redovima i u nove redove u ovisnosti kolko ih ima
            this.RowCount = (int)Math.Ceiling((decimal)opisiFilmova.Count / this.ColumnCount);
            this.Height = (int)(this.RowCount * visinaOmjer * (float)this.Width/this.ColumnCount);
            this.ColumnStyles.Clear();
            this.RowStyles.Clear();

            for (i = 0; i < this.RowCount; i++)
            {
                this.RowStyles.Add(new RowStyle(SizeType.Percent, (float)100 / this.RowCount));
            }

            for(i = 0; i < this.ColumnCount; i++)
            {
                this.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, (float)100 / this.ColumnCount));
            }

            i = 1;

            //Ubacivanje opisa filma u MoviePanel
            foreach(OpisFilma opisFilma in opisiFilmova)
            {
                red = i / this.ColumnCount;
                stupac = i - red*this.ColumnCount - 1;

                MoviePanel moviePanel = new MoviePanel(opisFilma.Naslov, opisFilma.Opis, opisFilma.Slika);
                this.Controls.Add(moviePanel, stupac, red);
                moviePanel.Dock = DockStyle.Fill;

                i++;
            }
        }
    }
}
