﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace KiNO2.View
{
    class MoviePanel:Panel
    {
        private PictureBox slikaPictureBox;
        private Label naslovLabel;
        private TextBox opisTextBox;

        public MoviePanel(string naslov, string opis, string slikaUri)
        {
            this.BackColor = Color.FromArgb(252,252,252);
            this.Padding = new Padding(5);

            //inicijalizacija
            this.slikaPictureBox = new PictureBox();
            this.naslovLabel = new Label();
            this.opisTextBox = new TextBox();

            //slikaPictureBox           
            this.Controls.Add(slikaPictureBox);
            slikaPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;           
            slikaPictureBox.Dock = DockStyle.Top;
            slikaPictureBox.Load(slikaUri);
            slikaPictureBox.Height =(int)(slikaPictureBox.Width * (float)slikaPictureBox.Image.Height / slikaPictureBox.Image.Width);

            //naslovLabel
            this.Controls.Add(naslovLabel);
            naslovLabel.Dock = DockStyle.Top;
            naslovLabel.Text = naslov;

            //opistextBox
            this.Controls.Add(opisTextBox);
            opisTextBox.BringToFront();
            opisTextBox.Dock = DockStyle.Fill;
            opisTextBox.Multiline = true;
            opisTextBox.ScrollBars = ScrollBars.Vertical;          
            opisTextBox.Text = opis;
        }
    }
}
