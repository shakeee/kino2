﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KiNO2
{
    public partial class frmKarta : Form
    {
        private int ID;
        public frmKarta(int id)
        {
            InitializeComponent();
            ID = id;
        }

        private void frmKarta_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the '_16058_DBDataSet.tblKarta' table. You can move, or remove it, as needed.
            this.tblKartaTableAdapter.FillbyRezervacijaID(this._16058_DBDataSet.tblKarta,ID);

            this.rptKarta.RefreshReport();
        }
    }
}
