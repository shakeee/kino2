﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KiNO2.Logic;
using KiNO2.HelperClasses;
using KiNO2;
using KiNO2.Properties;
using KiNO2.View;
using KiNO2.Entities;
using TMDbLib.Objects.General;
using TMDbLib.Client;
//Using external DLL
using KiNODLL;
using System.IO;

namespace KiNO2
{
    public partial class frmGlavnaForma : Form
    {
        
        public frmGlavnaForma()
        {
            InitializeComponent();
            File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory + "\\help.pdf", Resources.help);
        }

        private void frmGlavnaForma_Load(object sender, EventArgs e)
        {
            //Set all tabs same size on form lead
            tabControl.ItemSize = new Size(tabControl.Width / tabControl.TabCount, 0);
        }

        private void frmGlavnaForma_Resize(object sender, EventArgs e)
        {
            //Set all tabs same size on resize
            tabControl.ItemSize = new Size(tabControl.Width / tabControl.TabCount, 0);
        }
        #region Login
        private void btnLogin_Click(object sender, EventArgs e)
        {
            //Using external dll
            if (PrijavaLogic.autentifikacijaKorisnika(txtKorisnickoIme.Text, Crypto.sha256(txtLozinka.Text)) == 1) //Check here if user exists an if password matches the user
            {
                //If user exists
                pnlLoginPanel.Hide();
            }
            else if(PrijavaLogic.autentifikacijaKorisnika(txtKorisnickoIme.Text,txtLozinka.Text) == 2)
            {
                pnlLoginPanel.Hide();
                tabControl.TabPages.Remove(tabControl.TabPages["tabOsoblje"]);
                tabControl.TabPages.Remove(tabControl.TabPages["tabDvorane"]);
                tabControl.TabPages.Remove(tabControl.TabPages["tabPredstave"]);
                tabControl.TabPages.Remove(tabControl.TabPages["tabFilmovi"]);
            }
            else
            {
                MessageBox.Show("Unesena je pogrešna lozinka ili mail adresa!");
            }
        }
        #endregion

        //When tab changes
        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl.SelectedTab == tabControl.TabPages["tabDvorane"])
            {
                lbxTheterRoomsRefresh();
            }
            else if (tabControl.SelectedTab == tabControl.TabPages["tabRezervacije"])
            {
                reservationsRefresh();
            }
            else if (tabControl.SelectedTab == tabControl.TabPages["tabPomoc"])
            {
            System.Diagnostics.Process.Start(@"help.pdf");
            }
            else if(tabControl.SelectedTab == tabControl.TabPages["tabOsoblje"])
            {
            prikaziKategorijeZaposlenika();
            }
        }

        #region Dvorane

        private void btnAddNewTheatherRoom_Click(object sender, EventArgs e)
        {
            frmDvoraneForma frmDvoraneFormaNew = new frmDvoraneForma();
            frmDvoraneFormaNew.ShowDialog();
            lbxTheterRoomsRefresh();
        }

        private void lbxTheterRoomsRefresh()
        {
            List<Dvorana> theatherRoomsList = DvoraneLogic.getTheatherRoomsList();
            lbxTheaterRooms.DataSource = theatherRoomsList;
            lbxTheaterRooms.DisplayMember = "naziv";
            lbxTheaterRooms.ValueMember = "dvoranaId";
        }

        private void lbxTheaterRooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            Dvorana selectedThetherRoom = new Dvorana();
            selectedThetherRoom = lbxTheaterRooms.SelectedItem as Dvorana;
            List<Sjedalo> seatList = DvoraneLogic.getSeatsList(selectedThetherRoom);
            List<SjedaloHelper> seatHelperList = DvoraneLogic.returnSeatHelperList(seatList);
            changeTheatherRoomDimensions(selectedThetherRoom.duljina, selectedThetherRoom.sirina);

            pnlTheaterRoomEditor.Controls.Clear();
            foreach (var seatHelper in seatHelperList)
            {
                pnlTheaterRoomEditor.Controls.Add(seatHelper.image);
                seatHelper.image.Location = seatHelper.position;
            }
        }

        private void changeTheatherRoomDimensions(int numberOfRows, int numberOfColumns)
        {
            pnlTheaterRoomEditor.Size = new Size(numberOfColumns * 20 + 1, numberOfRows * 20 + 1);
            pnlTheaterRoomEditor.Left = (pnlTheaterRoom.ClientSize.Width - pnlTheaterRoomEditor.Width) / 2;
            pnlTheaterRoomEditor.Top = (pnlTheaterRoom.ClientSize.Height - pnlTheaterRoomEditor.Height) / 2 + 20;
        }

        private void btnEditTheaterRoom_Click(object sender, EventArgs e)
        {
            Dvorana selectedThetherRoom = new Dvorana();
            selectedThetherRoom = lbxTheaterRooms.SelectedItem as Dvorana;
            frmDvoraneForma frmDvoraneFormaNew = new frmDvoraneForma(selectedThetherRoom);
            frmDvoraneFormaNew.Show();
        }

        private void btnDeleteTheaterRoom_Click(object sender, EventArgs e)
        {
            Dvorana selectedThetherRoom = new Dvorana();
            selectedThetherRoom = lbxTheaterRooms.SelectedItem as Dvorana;
            DvoraneLogic.deleteTheaterRoom(selectedThetherRoom);
        }

        #endregion

        #region Rezervacije

        List<SjedaloHelper> seatHelperList = new List<SjedaloHelper>();
        List<SjedaloHelper> reservedSeatHelperList = new List<SjedaloHelper>();
        int numberOfTickets = 1;
        int numberOfSeatsChosen = 0;
        bool isReservationInfoEnabled = false;

        private void reservationsRefresh()
        {
            seatHelperList.Clear();
            reservedSeatHelperList.Clear();
            nudNumberOfTickets.Value = 1;
            tbxName.Text = "";
            tbxSurename.Text = "";
            tbxTelephone.Text = "";
            cbxMovies_SelectedIndexChanged(null, null);
            cbxTiming_SelectedIndexChanged(null, null);
            numberOfTickets = (int)nudNumberOfTickets.Value;
            numberOfSeatsChosen = 0;
        }

        private void changeTheatherRoomDimensionsReservations(int numberOfRows, int numberOfColumns)
        {
            pnlTheaterRoomEditorReservations.Size = new Size(numberOfColumns * 20 + 1, numberOfRows * 20 + 1);
            pnlTheaterRoomEditorReservations.Left = (pnlTheaterRoomReservations.ClientSize.Width - pnlTheaterRoomEditorReservations.Width) / 2;
            pnlTheaterRoomEditorReservations.Top = (pnlTheaterRoomReservations.ClientSize.Height - pnlTheaterRoomEditorReservations.Height) / 2 + 20;
        }

        //Refresh reservations info once date change
        private void calReservations_DateChanged(object sender, DateRangeEventArgs e)
        {
            cbxMovies.DataSource = RezervacijeLogic.getMoviesOnDateList(calReservations.SelectionRange.Start);
            cbxMovies.DisplayMember = "naziv";
        }

        //Refresh reservations info once movie changes
        private void cbxMovies_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxMovies.SelectedIndex > -1)
            {
                cbxTiming.DataSource = RezervacijeLogic.getShowsPerMovieList(cbxMovies.SelectedItem as Film, calReservations.SelectionRange.Start);
                cbxTiming.DisplayMember = "infoPredstave";
            }
        }

        //Refresh reservations info once timing changes
        private void cbxTiming_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxMovies.SelectedIndex > -1 && cbxTiming.SelectedIndex > -1)
            {
                Dvorana selectedThetherRoom = new Dvorana();

                using (var db = new KinoDBContext())
                {
                    db.Predstave.Attach((cbxTiming.SelectedItem as Predstava));
                    selectedThetherRoom = (cbxTiming.SelectedItem as Predstava).Dvorana;
                    seatHelperList = RezervacijeLogic.returnReservedSeatHelperList(cbxTiming.SelectedItem as Predstava);

                    changeTheatherRoomDimensionsReservations(selectedThetherRoom.duljina, selectedThetherRoom.sirina);

                    pnlTheaterRoomEditorReservations.Controls.Clear();
                    foreach (var seatHelper in seatHelperList)
                    {
                        seatHelper.image.Click += new EventHandler(pictureBoxClick);
                        pnlTheaterRoomEditorReservations.Controls.Add(seatHelper.image);
                        seatHelper.image.Location = seatHelper.position;
                    }
                }
            }
        }

        private void pictureBoxClick(object sender, EventArgs e)
        {
            PictureBox selectedSeatPictureBox = (PictureBox)sender;
            Point selectedSeatLocation = selectedSeatPictureBox.Location;
            SjedaloHelper selectedSeat = seatHelperList.Find(s => s.position == selectedSeatLocation);
            if (selectedSeat.seatTypeId == 1 && selectedSeat.seatReserved == false)
            {

                if (reservedSeatHelperList.Contains(selectedSeat))
                {
                    selectedSeatPictureBox.Image = Resources.s;
                    reservedSeatHelperList.Remove(selectedSeat);
                    numberOfTickets++;
                    numberOfSeatsChosen--;
                }
                else
                {
                    if (numberOfTickets >= 1)
                    {
                        selectedSeatPictureBox.Image = Resources.sc;
                        reservedSeatHelperList.Add(selectedSeat);
                        numberOfTickets--;
                        numberOfSeatsChosen++;
                    }
                    else
                    {
                        if (isReservationInfoEnabled == true)
                        {
                            Rezervacija reservation = RezervacijeLogic.getReservation((cbxTiming.SelectedItem as Predstava), selectedSeat);
                            MessageBox.Show("Informacije o rezervaciji:"
                                + Environment.NewLine + "Broj rezervacije: " + reservation.rezervacijaId.ToString()
                                + Environment.NewLine + "Osoba: " + reservation.ime + " " + reservation.prezime
                                + Environment.NewLine + "Broj telefona: " + reservation.telefon
                                + Environment.NewLine + "Karta kupljena: " + reservation.kupljeno.ToString());
                        }
                        else
                        {
                            MessageBox.Show("Ne možete rezervirati već zauzeto sjedalo!");
                        }
                    }
                }

            }
            else if (selectedSeat.seatTypeId == 2 && selectedSeat.seatReserved == false)
            {

                if (reservedSeatHelperList.Contains(selectedSeat))
                {
                    selectedSeatPictureBox.Image = Resources.l;
                    reservedSeatHelperList.Remove(selectedSeat);
                    numberOfTickets += 2;
                    numberOfSeatsChosen -= 2;
                }
                else
                {
                    if (numberOfTickets >= 2)
                    {
                        selectedSeatPictureBox.Image = Resources.lc;
                        reservedSeatHelperList.Add(selectedSeat);
                        numberOfTickets -= 2;
                        numberOfSeatsChosen += 2;
                    }
                    else
                    {
                        MessageBox.Show("Nedovoljan broj željenih karata!");
                    }
                }
            }
            else
            {
                if (isReservationInfoEnabled == true)
                {
                    Rezervacija reservation = RezervacijeLogic.getReservation((cbxTiming.SelectedItem as Predstava), selectedSeat);

                    DialogResult dialogResult = MessageBox.Show(
                        "Informacije o rezervaciji:"
                        + Environment.NewLine + "Broj rezervacije: " + reservation.rezervacijaId.ToString()
                        + Environment.NewLine + "Osoba: " + reservation.ime + " " + reservation.prezime
                        + Environment.NewLine + "Broj telefona: " + reservation.telefon
                        + Environment.NewLine + "Cijena rezervacije: " + Math.Round(reservation.cijena, 2).ToString() + " kn"
                        + Environment.NewLine + "Karte kupljene: " + reservation.kupljeno.ToString(),
                        "Želite li stornirati rezervaciju?",
                        MessageBoxButtons.YesNo);

                    if (dialogResult == DialogResult.Yes)
                    {
                        RezervacijeLogic.deleteReservation(reservation);
                        reservationsRefresh();
                    }
                }
                else
                {
                    MessageBox.Show("Ne možete rezervirati već zauzeto sjedalo!");
                }
            }
        }


        private void nudNumberOfTickets_ValueChanged(object sender, EventArgs e)
        {
            numberOfTickets = (int)nudNumberOfTickets.Value;
            foreach (var selectedSeat in reservedSeatHelperList)
            {
                if (selectedSeat.seatTypeId == 1)
                {
                    selectedSeat.image.Image = Resources.s;
                }
                else if (selectedSeat.seatTypeId == 2)
                {
                    selectedSeat.image.Image = Resources.l;
                }
            }
            reservedSeatHelperList.Clear();
        }

        private void btnMakeReservation_Click(object sender, EventArgs e)
        {
            if (tbxName.Text == "")
            {
                MessageBox.Show("Morate popuniti Ime na koje se rezrvacija vodi!");
            }
            else if (tbxSurename.Text == "")
            {
                MessageBox.Show("Morate popuniti Prezime na koje se rezrvacija vodi!");
            }
            else if (tbxTelephone.Text == "")
            {
                MessageBox.Show("Morate popuniti Telefon!");
            }
            else if (numberOfSeatsChosen != (int)nudNumberOfTickets.Value)
            {
                MessageBox.Show("Niste odabrali sva sjedala!");
            }
            else
            {
                Rezervacija newReservation = RezervacijeLogic.saveReservation((cbxTiming.SelectedItem as Predstava), tbxName.Text, tbxSurename.Text, tbxTelephone.Text);
                RezervacijeLogic.saveSeats(reservedSeatHelperList, newReservation, numberOfSeatsChosen);
                reservationsRefresh();
            }

        }


        private void btnReservationsList_Click(object sender, EventArgs e)
        {
            if (isReservationInfoEnabled == true)
            {
                isReservationInfoEnabled = false;
                pnlTheaterRoomEditorReservations.Cursor = Cursors.Default;
            }
            else
            {
                isReservationInfoEnabled = true;
                pnlTheaterRoomEditorReservations.Cursor = Cursors.Help;
            }
        }

        #endregion

        private void btnBuyTickets_Click(object sender, EventArgs e)
        {
            if (tbxName.Text == "")
            {
                MessageBox.Show("Morate popuniti Ime na koje se rezrvacija vodi!");
            }
            else if (tbxSurename.Text == "")
            {
                MessageBox.Show("Morate popuniti Prezime na koje se rezrvacija vodi!");
            }
            else if (tbxTelephone.Text == "")
            {
                MessageBox.Show("Morate popuniti Telefon!");
            }
            else if (numberOfSeatsChosen != (int)nudNumberOfTickets.Value)
            {
                MessageBox.Show("Niste odabrali sva sjedala!");
            }
            else
            {
                Rezervacija newReservation = RezervacijeLogic.saveReservation((cbxTiming.SelectedItem as Predstava), tbxName.Text, tbxSurename.Text, tbxTelephone.Text, true);
                RezervacijeLogic.saveSeats(reservedSeatHelperList, newReservation, numberOfSeatsChosen);
                reservationsRefresh();
            }
        }
        
        #region Osoblje
        
        private void dgvKategorijaZaposlenika_SelectionChanged(object sender, EventArgs e)
        {
            KategorijaZaposlenika odabranaKategorija = kategorijaZaposlenikaBindingSource.Current as KategorijaZaposlenika;
            if(odabranaKategorija != null)
            {
                prikaziZaposlenike(odabranaKategorija);
            }
        }

        private void prikaziKategorijeZaposlenika()
        {
            BindingList<KategorijaZaposlenika> listaKategorija = null;
            using (var db = new KinoDBContext())
            {
                listaKategorija = new BindingList<KategorijaZaposlenika>(db.KategorijeZaposlenika.ToList());
            }
            kategorijaZaposlenikaBindingSource.DataSource = listaKategorija;
        }

        private void prikaziZaposlenike(KategorijaZaposlenika kategorija)
        {
            BindingList<Zaposlenik> listaZaposlenika = null;

            using (var db = new KinoDBContext() )
            {
                db.KategorijeZaposlenika.Attach(kategorija);
                listaZaposlenika = new BindingList<Zaposlenik>(kategorija.Zaposlenici.ToList());
            }
            zaposlenikBindingSource1.DataSource = listaZaposlenika;
        }

        Zaposlenik odabraniZaposlenik;

        private void dgvZaposlenici_SelectionChanged(object sender, EventArgs e)
        {
            odabraniZaposlenik = zaposlenikBindingSource1.Current as Zaposlenik;
        }

        private void btnDodajKorisnika_Click(object sender, EventArgs e)
        {
            frmDodavanjeKorisnika dodajNovog = new frmDodavanjeKorisnika();
            dodajNovog.ShowDialog();
            prikaziKategorijeZaposlenika();
            prikaziZaposlenike(kategorijaZaposlenikaBindingSource.Current as KategorijaZaposlenika);
        }

        private void btnAzuriraj_Click(object sender, EventArgs e)
        {
            frmAžuriranjeKorisnika azuriraj = new frmAžuriranjeKorisnika(odabraniZaposlenik);
            azuriraj.ShowDialog();
            prikaziZaposlenike(kategorijaZaposlenikaBindingSource.Current as KategorijaZaposlenika);
        }

        private void btnUkloniKorisnika_Click(object sender, EventArgs e)
        {
            ukloniKorisnika(odabraniZaposlenik);
            prikaziZaposlenike(kategorijaZaposlenikaBindingSource.Current as KategorijaZaposlenika);
        }

        private void ukloniKorisnika(Zaposlenik odabraniZaposlenik)
        {
            if (MessageBox.Show("Jeste li sigurni da želite ukloniti odabrani zapis?", "Upozorenje!", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                using (var db = new KinoDBContext())
                {
                    db.Zaposlenici.Attach(odabraniZaposlenik);
                    db.Zaposlenici.Remove(odabraniZaposlenik);
                    db.SaveChanges();
                }
            }
        }
        #endregion 

        #region Filmovi
        private void button4_Click(object sender, EventArgs e)
        {
            string zanr = ((Button)sender).Tag.ToString();
            List<OpisFilma> filmovi = FilmoviLogic.DohvatiFilmByZanr(zanr);
            movieTableLayoutPanel1.ShowMovies(filmovi);
        }

        private void btnDodajNoviFilm_Click(object sender, EventArgs e)
        {
            UnosFilma unesi = new UnosFilma();
            unesi.Show();
        }

        private void btnUkloniFilmIzBaze_Click(object sender, EventArgs e)
        {
            UkloniFilm ukloni = new UkloniFilm();
            ukloni.Show();
        }
        #endregion

        #region Predstave
        private void predDatumVrijeme_ValueChanged(object sender, EventArgs e)
        {
            predstavaBindingSource.DataSource = PredstaveLogic.DohvatPredstava(predDatumVrijeme.Value.Date);
        }

        private void btnNovaPredstava_Click(object sender, EventArgs e)
        {
            Nova_predstava frmPred = new Nova_predstava();
            frmPred.ShowDialog();
        }
        
        private void btnUkloniPredstavu_Click(object sender, EventArgs e)
        {
            using (var db = new KinoDBContext())
            {
                Predstava predstava = db.Predstave.Find(((OpisPredstava)dataGridView1.SelectedRows[0].DataBoundItem).id);
                try
                {
                    //foreach (var rezervacija in predstava.Rezervacije)
                    //{
                    //    for (int i = 0; i < rezervacija.RezerviranaSjedala.Count(); i++)
                    //    {
                    //        db.Entry(rezervacija.RezerviranaSjedala.ToList()[0]).State = System.Data.Entity.EntityState.Deleted;
                    //    }
                    //}

                    //for (int i = 0; i < predstava.Rezervacije.Count(); i++)
                    //{
                    //    db.Entry(predstava.Rezervacije.ToList()[0]).State = System.Data.Entity.EntityState.Deleted;
                    //}
                    PredstaveLogic.UkloniPredstavu(predstava);

                    predstava.Rezervacije.Clear();


                    db.SaveChanges();

                    db.Entry(predstava).State = System.Data.Entity.EntityState.Deleted;
                    db.SaveChanges();


                }
                catch (Exception)
                {
                    MessageBox.Show("Ne možete obrisati tu predstavu jer su za nju već zakupljena sjedala!");
                }

            }
            dataGridView1.DataSource = predstavaBindingSource;
            predstavaBindingSource.DataSource = PredstaveLogic.DohvatPredstava(predDatumVrijeme.Value.Date);

        }
        #endregion

        #region Karte
        private void btnTrazi_Click(object sender, EventArgs e)
        {
            if(txtBrojRezervacije.Text == "")
            {
                listBox1.DataSource = KarteLogic.TraziBrojRezervacije(txtNaruciteljIme.Text, txtNaruciteljPrezime.Text);
            }
            else
            {
                listBox1.DataSource = KarteLogic.TraziBrojRezervacije(int.Parse(txtBrojRezervacije.Text));
            }
            
            listBox1.DisplayMember = "infoRezervacija";
        }
        
        private void btnKupi_Click(object sender, EventArgs e)
        {
            Rezervacija rezervacija = listBox1.SelectedItem as Rezervacija;
            KarteLogic.KupiRezervaciju(rezervacija);
            frmKarta karta = new frmKarta(rezervacija.rezervacijaId);
            karta.ShowDialog();
            MessageBox.Show("Rezervacija kupljena!");
            btnTrazi_Click(null, null);

        }

        private void frmGlavnaForma_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F1)
            {
                System.Diagnostics.Process.Start(@"help.pdf");
            }
        }
        #endregion
    }
}
