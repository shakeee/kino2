namespace KiNO2
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class KinoDBContext : DbContext
    {
        public KinoDBContext()
            : base("name=KinoDBContext")
        {
        }

        public virtual DbSet<Dvorana> Dvorane { get; set; }
        public virtual DbSet<TipSjedala> TipoviSjedala { get; set; }
        public virtual DbSet<Sjedalo> Sjedala { get; set; }
        public virtual DbSet<Film> Filmovi { get; set; }
        public virtual DbSet<KategorijaZaposlenika> KategorijeZaposlenika { get; set; }
        public virtual DbSet<Predstava> Predstave { get; set; }
        public virtual DbSet<TipPredstave> TipoviPredstava { get; set; }
        public virtual DbSet<Rezervacija> Rezervacije { get; set; }
        public virtual DbSet<RezerviranoSjedalo> RezerviranaSjedala { get; set; }
        public virtual DbSet<Zaposlenik> Zaposlenici { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<KategorijaZaposlenika>()
                .HasMany(e => e.Zaposlenici)
                .WithRequired(e => e.KategorijaZaposlenika)
                .HasForeignKey(e => e.kategorijaZaposlenikaId);

            modelBuilder.Entity<Rezervacija>()
                .Property(e => e.cijena)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TipPredstave>()
                .Property(e => e.cijenaPredstave)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Rezervacija>()
                .HasMany(e => e.RezerviranaSjedala)
                .WithRequired(e => e.Rezervacija)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Zaposlenik>()
                .Property(e => e.lozinka)
                .IsFixedLength();                
        }
    }
}
