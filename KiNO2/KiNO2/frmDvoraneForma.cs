﻿using KiNO2.HelperClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KiNO2.Logic;

namespace KiNO2
{
    public partial class frmDvoraneForma : Form
    {
        List<SjedaloHelper> seatHelperList = new List<SjedaloHelper>();
        public Dvorana newTheatherRoom = new Dvorana();
        public Dvorana theatherRoom = new Dvorana();

        bool deleteOptionTurnedOn = false;

        public frmDvoraneForma(Dvorana theatherRoom = null)
        {
            InitializeComponent();
            this.theatherRoom = theatherRoom;
        }

        private void frmDvoraneForma_Load(object sender, EventArgs e)
        {
            if (theatherRoom == null)
            {
                this.newTheatherRoom.naziv = "TempNaziv";
                tbxNameOfTheatherRoom.Text = this.newTheatherRoom.naziv;
                nudNumberOfColumns.Value = 15;
                nudNumberOfRows.Value = 15;
                this.newTheatherRoom = DvoraneLogic.saveTheatherRoom(this.newTheatherRoom);
            }
            else
            {
                this.newTheatherRoom = theatherRoom;
                tbxNameOfTheatherRoom.Text = this.newTheatherRoom.naziv;
                nudNumberOfColumns.Value = this.newTheatherRoom.sirina;
                nudNumberOfRows.Value = this.newTheatherRoom.duljina;
            }

            seatHelperList = DvoraneLogic.returnSeatHelperList(this.newTheatherRoom.Sjedala.ToList());

            changeTheatherRoomDimensions();

            foreach (var seatHelper in seatHelperList)
            {
                seatHelper.image.Click += new EventHandler(pictureBoxClick);
                pnlTheaterRoomEditor.Controls.Add(seatHelper.image);
                seatHelper.image.Location = seatHelper.position;
            }
        }

        private void changeTheatherRoomDimensions()
        {
            pnlTheaterRoomEditor.Size = new Size((int)nudNumberOfColumns.Value * 20 + 1, (int)nudNumberOfRows.Value * 20 + 1);
            Bitmap bmp = new Bitmap(pnlTheaterRoomEditor.ClientSize.Width, pnlTheaterRoomEditor.ClientSize.Height);
            Size gridSpacing = new Size(20, 20);
            using (Graphics G = Graphics.FromImage(bmp))
            {
                ControlPaint.DrawGrid(G, new Rectangle(Point.Empty, bmp.Size), gridSpacing, Color.White);
            }
            pnlTheaterRoomEditor.BackgroundImage = bmp;

            pnlTheaterRoomEditor.Left = (pnlTheaterRoom.ClientSize.Width - pnlTheaterRoomEditor.Width) / 2;
            pnlTheaterRoomEditor.Top = (pnlTheaterRoom.ClientSize.Height - pnlTheaterRoomEditor.Height) / 2 + 20;
        }

        private void nudNumberOfRows_ValueChanged(object sender, EventArgs e)
        {
            changeTheatherRoomDimensions();
        }

        private void nudNumberOfColumns_ValueChanged(object sender, EventArgs e)
        {
            changeTheatherRoomDimensions();
        }

        private void pnlTheaterRoomEditor_MouseMove(object sender, MouseEventArgs e)
        {
            if (seatHelperList.Count > 0)
            {
                if (seatHelperList.Last().seatPositionSet == false)
                {
                    Point point = pnlTheaterRoomEditor.PointToClient(Cursor.Position);
                    point = new Point(((int)point.X / 20) * 20, ((int)point.Y / 20) * 20);
                    seatHelperList.Last().image.Location = point;
                    seatHelperList.Last().position = point;
                }
            }
        }

        private void addSeatToHelperList(int tipSjedalaId, Bitmap prikaz)
        {
            if (seatHelperList.Count > 0)
            {
                if (seatHelperList.Last().seatPositionSet == true)
                {
                    SjedaloHelper novoSjedaloHelper = new SjedaloHelper();
                    novoSjedaloHelper.seatTypeId = tipSjedalaId;
                    PictureBox pictureBox = new PictureBox();
                    pictureBox.Image = prikaz;
                    pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
                    pictureBox.Click += new EventHandler(pictureBoxClick);
                    novoSjedaloHelper.image = pictureBox;
                    seatHelperList.Add(novoSjedaloHelper);
                }
                else
                {
                    seatHelperList.Remove(seatHelperList.Last());
                    SjedaloHelper novoSjedaloHelper = new SjedaloHelper();
                    novoSjedaloHelper.seatTypeId = tipSjedalaId;
                    PictureBox pictureBox = new PictureBox();
                    pictureBox.Image = prikaz;
                    pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
                    pictureBox.Click += new EventHandler(pictureBoxClick);
                    novoSjedaloHelper.image = pictureBox;
                    seatHelperList.Add(novoSjedaloHelper);
                }
            }
            else
            {
                SjedaloHelper novoSjedaloHelper = new SjedaloHelper();
                novoSjedaloHelper.seatTypeId = tipSjedalaId;
                PictureBox pictureBox = new PictureBox();
                pictureBox.Image = prikaz;
                pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
                pictureBox.Click += new EventHandler(pictureBoxClick);
                novoSjedaloHelper.image = pictureBox;
                seatHelperList.Add(novoSjedaloHelper);
            }
        }

        private void pictureBoxClick(object sender, EventArgs e)
        {
            //Removinig seat
            if (deleteOptionTurnedOn == true)
            {
                PictureBox selectedSeatPictureBox = (PictureBox)sender;
                Point selectedSeatLocation = selectedSeatPictureBox.Location;
                SjedaloHelper selectedSeat = seatHelperList.Find(s => s.position == selectedSeatLocation);
                pnlTheaterRoomEditor.Controls.Remove(selectedSeatPictureBox);
                seatHelperList.Remove(selectedSeat);
            }
            else
            {
                if (seatHelperList.Last().seatPositionSet == false)
                {
                    seatHelperList.Last().seatPositionSet = true;
                }
                SjedaloHelper novoSjedalo = new SjedaloHelper();
                novoSjedalo.seatTypeId = seatHelperList.Last().seatTypeId;
                PictureBox pictureBox = new PictureBox();
                pictureBox.Image = seatHelperList.Last().image.Image;
                pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
                pictureBox.Click += new EventHandler(pictureBoxClick);
                novoSjedalo.image = pictureBox;
                seatHelperList.Add(novoSjedalo);
            }
        }

        private void pnlTheaterRoomEditor_MouseEnter(object sender, EventArgs e)
        {
            if (seatHelperList.Count > 0)
            {
                if (seatHelperList.Last().seatPositionSet == false)
                {
                    pnlTheaterRoomEditor.Controls.Add(seatHelperList.Last().image);
                }
            }
        }

        private void pbxSingleSeat_Click(object sender, EventArgs e)
        {
            deleteOptionTurnedOn = false;
            pnlTheaterRoomEditor.Cursor = Cursors.Cross;
            if (seatHelperList.Count > 0)
            {
                if (seatHelperList.Last().seatPositionSet == false)
                {
                    pnlTheaterRoomEditor.Controls.Remove(seatHelperList.Last().image);
                    seatHelperList.Remove(seatHelperList.Last());
                }
            }
            addSeatToHelperList(1, Properties.Resources.s);
        }

        private void pbxLoversSeat_Click(object sender, EventArgs e)
        {
            deleteOptionTurnedOn = false;
            pnlTheaterRoomEditor.Cursor = Cursors.Cross;
            if (seatHelperList.Count > 0)
            {
                if (seatHelperList.Last().seatPositionSet == false)
                {
                    pnlTheaterRoomEditor.Controls.Remove(seatHelperList.Last().image);
                    seatHelperList.Remove(seatHelperList.Last());
                }
            }
            addSeatToHelperList(2, Properties.Resources.l);
        }

        private void btnSaveTheatherRoom_Click(object sender, EventArgs e)
        {
            if (seatHelperList.Count > 0)
            {
                if (seatHelperList.Last().seatPositionSet == false)
                {
                    pnlTheaterRoomEditor.Controls.Remove(seatHelperList.Last().image);
                    seatHelperList.Remove(seatHelperList.Last());
                }
            }

            newTheatherRoom.naziv = tbxNameOfTheatherRoom.Text;
            newTheatherRoom.sirina = Convert.ToInt32(Math.Round(nudNumberOfColumns.Value, 0));
            newTheatherRoom.duljina = Convert.ToInt32(Math.Round(nudNumberOfRows.Value, 0));
            DvoraneLogic.saveTheatherRoom(newTheatherRoom);
            DvoraneLogic.saveSeats(seatHelperList, newTheatherRoom);
            this.Close();
        }

        private void frmDvoraneForma_FormClosing(object sender, FormClosingEventArgs e)
        {
            DvoraneLogic.deleteUnsavedChanges(newTheatherRoom);
        }

        private void pbxDeleteSeat_Click(object sender, EventArgs e)
        {
            if (seatHelperList.Count > 0)
            {
                if (seatHelperList.Last().seatPositionSet == false)
                {
                    pnlTheaterRoomEditor.Controls.Remove(seatHelperList.Last().image);
                    seatHelperList.Remove(seatHelperList.Last());
                }
            }

            if (deleteOptionTurnedOn == false)
            {
                pnlTheaterRoomEditor.Cursor = Cursors.Hand;
                deleteOptionTurnedOn = true;
            }
            else
            {
                pnlTheaterRoomEditor.Cursor = Cursors.Default;
                deleteOptionTurnedOn = false;
            }           

        }

        private void frmDvoraneForma_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F1)
            {
                System.Diagnostics.Process.Start(@"help.pdf");
            }
        }
    }
}
