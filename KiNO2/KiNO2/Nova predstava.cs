﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KiNO2.Entities;
using KiNO2.Logic;

namespace KiNO2
{
    public partial class Nova_predstava : Form
    {
        public Nova_predstava()
        {
            InitializeComponent();
        }

        private void btnNoPredSpremi_Click(object sender, EventArgs e)
        {
            PredstaveLogic.SpremiPredstavu((int)(cmBoxFilmovi.SelectedValue), (int)cmBoxDvorane.SelectedValue, (int)cmBoxZaposlenik.SelectedValue, DatumVrijemeOdaberi.Value.Date, DatumVrijemeOdaberi.Value.TimeOfDay, (int)cmBoxTipPredstave.SelectedValue);
            Close();
        }

        private void Nova_predstava_Load(object sender, EventArgs e)
        {
            BindingList<Film> listaFilmova = PredstaveLogic.PrikaziFIlmove();
            filmBindingSource.DataSource = listaFilmova;

            BindingList<Zaposlenik> listaZaposlenika = PredstaveLogic.PrikaziZaposlenike();
            zaposlenikBindingSource.DataSource = listaZaposlenika;

            BindingList<Dvorana> listaDvorana = PredstaveLogic.PrikaziDvorane();
            dvoranaBindingSource.DataSource = listaDvorana;

            BindingList<TipPredstave> listaTipaPred = PredstaveLogic.PrikaziTipPredstave();
            tipPredstaveBindingSource.DataSource = listaTipaPred;
        }

        private void DatumVrijemeOdaberi_ValueChanged(object sender, EventArgs e)
        {

            cmBoxDvorane.Enabled = true;
        }

        private void Nova_predstava_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F1)
            {
                System.Diagnostics.Process.Start(@"help.pdf");
            }
        }
    }
}
