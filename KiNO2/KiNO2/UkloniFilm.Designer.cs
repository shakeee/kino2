﻿namespace KiNO2
{
    partial class UkloniFilm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.filmBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnUkloni = new System.Windows.Forms.Button();
            this.lFilmovaZaUklonit = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.filmBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // filmBindingSource
            // 
            this.filmBindingSource.DataSource = typeof(KiNO2.Film);
            // 
            // btnUkloni
            // 
            this.btnUkloni.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUkloni.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnUkloni.FlatAppearance.BorderSize = 0;
            this.btnUkloni.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.btnUkloni.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUkloni.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnUkloni.Location = new System.Drawing.Point(12, 256);
            this.btnUkloni.Name = "btnUkloni";
            this.btnUkloni.Size = new System.Drawing.Size(147, 37);
            this.btnUkloni.TabIndex = 1;
            this.btnUkloni.Text = "Ukloni Film";
            this.btnUkloni.UseVisualStyleBackColor = false;
            this.btnUkloni.Click += new System.EventHandler(this.btnUkloni_Click);
            // 
            // lFilmovaZaUklonit
            // 
            this.lFilmovaZaUklonit.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lFilmovaZaUklonit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lFilmovaZaUklonit.DataSource = this.filmBindingSource;
            this.lFilmovaZaUklonit.DisplayMember = "naziv";
            this.lFilmovaZaUklonit.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lFilmovaZaUklonit.FormattingEnabled = true;
            this.lFilmovaZaUklonit.Location = new System.Drawing.Point(13, 22);
            this.lFilmovaZaUklonit.Name = "lFilmovaZaUklonit";
            this.lFilmovaZaUklonit.Size = new System.Drawing.Size(375, 195);
            this.lFilmovaZaUklonit.TabIndex = 2;
            this.lFilmovaZaUklonit.ValueMember = "filmId";
            // 
            // UkloniFilm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(400, 309);
            this.Controls.Add(this.lFilmovaZaUklonit);
            this.Controls.Add(this.btnUkloni);
            this.KeyPreview = true;
            this.Name = "UkloniFilm";
            this.Text = "UkloniFilm";
            this.Load += new System.EventHandler(this.UkloniFilm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UkloniFilm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.filmBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource filmBindingSource;
        private System.Windows.Forms.Button btnUkloni;
        private System.Windows.Forms.ListBox lFilmovaZaUklonit;
    }
}