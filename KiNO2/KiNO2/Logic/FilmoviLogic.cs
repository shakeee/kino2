﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using KiNO2.View;
using KiNO2.Entities;
using TMDbLib;
using TMDbLib.Objects.General;
using TMDbLib.Client;
using System.Windows.Forms;
using TMDbLib.Objects.Search;
using TMDbLib.Objects.Movies;

namespace KiNO2.Logic
{
    static partial class FilmoviLogic
    {
        //Traženje filma prema unesenoj riječi u textboxu
        public static List<Film> traziFIlm(string tekst)
        {
            //Korištenje tmdblib api-ja za dohvačanje traženih filmova
            TMDbClient client = new TMDbClient("6af5b16ff389e8c39110a04976170097");
            client.GetConfig();
            SearchContainer<SearchMovie> results = client.SearchMovie(tekst);
            List<Film> listaFilmova = new List<Film>();

            foreach (SearchMovie result in results.Results)
            {
                string zanrIds = "";
                Film film = new Film();
                ImagesWithId images = client.GetMovieImages(result.Id);

                film.naziv = result.Title;
                film.tmdbID = result.Id;
                film.opis = result.Overview;

                try
                {
                    film.slikaUri = client.GetImageUrl("original", images.Posters[0].FilePath).ToString();
                }
                catch (Exception)
                {
                }
                foreach (int zanrId in result.GenreIds)
                {
                    zanrIds += zanrId.ToString() + ",";
                }
                if (zanrIds.Length > 0)
                {
                    zanrIds.Remove(zanrIds.Length - 1, 1);
                }

                film.zanrId = zanrIds;
                listaFilmova.Add(film);
            }
            return listaFilmova;
        }

        //Unos odabranog filma iz listboxa u bazu filmova
        public static void UnosFilma(object trazeno)
        {
            try
            {
                using (var db = new KinoDBContext())
                {
                    Film film = (Film)trazeno;
                    db.Filmovi.Add(film);
                    db.SaveChanges();
                    MessageBox.Show("Film je uspješno dodan!");
                }
            }
            catch
            {
                MessageBox.Show("Doslo je do greske u dodavanju");
            }
        }

        //Metoda koja vraća listu filmova prema odabranom žanru
        public static List<OpisFilma> DohvatiFilmByZanr(string zanr)
        {
            //u list<genre> allGenres dohvaca sve zanrove iz tmdb, pa uzme zanrove za cije je ime jednak proslijedeni zanr, 
            List<OpisFilma> opisi = new List<OpisFilma>();
            TMDbClient client = new TMDbClient("6af5b16ff389e8c39110a04976170097");
            List<Genre> allGenres = client.GetMovieGenres();
            List<Genre> genres = allGenres.Where(g => g.Name == zanr).ToList<Genre>();
            int id;

            if (genres.Count() > 0)
            {
                id = genres[0].Id;
            }
            else
            {
                return opisi;
            }

            using (var db = new KinoDBContext())
            {
                foreach (Film film in db.Filmovi)
                {
                    if (film.zanrId.Split(',').Contains(id.ToString()))
                    {
                        opisi.Add(new OpisFilma(film.naziv, film.opis, film.slikaUri));
                    }
                }
            }
            return opisi;
        }

        //Vraća listu filmova koji se nalaze u bazi u formi UkloniFilm
        public static BindingList<Film> PrikaziFIlmove()
        {
            BindingList<Film> listaFilmova = null;
            using (var db = new KinoDBContext())
            {
                listaFilmova = new BindingList<Film>(db.Filmovi.ToList());
            }
            return listaFilmova;
        }

        //Metoda koja služi za uklananje filmova iz baze
        public static void UkloniFilmove(Film selFilm)
        {
            if (selFilm != null)
            {
                if (MessageBox.Show("Da li ste sigurni?", "upozorenje", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    using (var db = new KinoDBContext())
                    {
                        db.Filmovi.Attach(selFilm);
                        db.Filmovi.Remove(selFilm);
                        db.SaveChanges();
                    }
                }
            }
        }
    }
}
