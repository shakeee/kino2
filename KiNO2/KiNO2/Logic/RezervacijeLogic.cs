﻿using KiNO2.HelperClasses;
using KiNO2.Properties;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KiNO2.Logic
{
    static partial class RezervacijeLogic
    {
        public static List<Film> getMoviesOnDateList(DateTime dateTime)
        {
            List<Film> moviesList = new List<Film>();
            using (var db = new KinoDBContext())
            {
                var query = from Film in db.Filmovi
                            join Predstava in db.Predstave on Film.filmId equals Predstava.filmId
                            where Predstava.datum == dateTime.Date
                            select Film;

                moviesList = query.Distinct().ToList<Film>();
                return moviesList;
            }
        }

        public static List<Predstava> getShowsPerMovieList(Film movie, DateTime dateTime)
        {
            List<Predstava> showsList = new List<Predstava>();
            using (var db = new KinoDBContext())
            {
                var query = from Predstava in db.Predstave
                            where Predstava.datum == dateTime.Date && Predstava.filmId == movie.filmId
                            select Predstava;

                showsList = query.Distinct().ToList<Predstava>();
                return showsList;
            }
        }

        public static List<Sjedalo> getSeatsList(Dvorana theatherRoom)
        {
            using (var db = new KinoDBContext())
            {
                return theatherRoom.Sjedala.ToList();
            }
        }

        public static List<SjedaloHelper> returnReservedSeatHelperList(Predstava show)
        {
            List<SjedaloHelper> tempSeatHelperList = new List<SjedaloHelper>();
            List<Sjedalo> reservedSeatList = new List<Sjedalo>();
            List<Sjedalo> seatList = new List<Sjedalo>();

            using (var db = new KinoDBContext())
            {
                //db.Predstave.Attach(show);
                seatList = show.Dvorana.Sjedala.ToList();
                foreach (var reservation in show.Rezervacije.ToList())
                {
                    foreach (var reservedSeat in reservation.RezerviranaSjedala)
                    {
                        reservedSeatList.Add(reservedSeat.Sjedalo);
                    }
                }

            }

            foreach (var seat in reservedSeatList)
            {
                seatList.Remove(seatList.Where(s => s.sjedaloId == seat.sjedaloId).FirstOrDefault());
            }

            foreach (var seat in seatList)
            {
                SjedaloHelper seatHelper = new SjedaloHelper();

                seatHelper.seatId = seat.sjedaloId;
                seatHelper.seatPositionSet = true;
                seatHelper.position = new Point(seat.pozicijaX, seat.pozicijaY);
                seatHelper.seatTypeId = seat.tipSjedalaId;
                seatHelper.seatReserved = false;

                seatHelper.image = new PictureBox();
                seatHelper.image.SizeMode = PictureBoxSizeMode.AutoSize;

                if (seatHelper.seatTypeId == 1)
                {
                    seatHelper.image.Image = Resources.s;
                }
                else if (seatHelper.seatTypeId == 2)
                {
                    seatHelper.image.Image = Resources.l;
                }

                tempSeatHelperList.Add(seatHelper);
            }

            foreach (var seat in reservedSeatList)
            {
                SjedaloHelper seatHelper = new SjedaloHelper();

                seatHelper.seatId = seat.sjedaloId;
                seatHelper.seatPositionSet = true;
                seatHelper.position = new Point(seat.pozicijaX, seat.pozicijaY);
                seatHelper.seatTypeId = seat.tipSjedalaId;
                seatHelper.seatReserved = true;

                seatHelper.image = new PictureBox();
                seatHelper.image.SizeMode = PictureBoxSizeMode.AutoSize;

                if (seatHelper.seatTypeId == 1)
                {
                    if (seatHelper.seatReserved == true)
                    {
                        seatHelper.image.Image = Resources.sr;
                    }
                    else
                    {
                        seatHelper.image.Image = Resources.s;
                    }

                }
                else if (seatHelper.seatTypeId == 2)
                {
                    if (seatHelper.seatReserved == true)
                    {
                        seatHelper.image.Image = Resources.lr;
                    }
                    else
                    {
                        seatHelper.image.Image = Resources.l;
                    }
                }

                tempSeatHelperList.Add(seatHelper);
            }

            return tempSeatHelperList;
        }

        public static List<RezerviranoSjedalo> returnSeatList(List<SjedaloHelper> seatHelperList, Rezervacija reservation)
        {
            List<RezerviranoSjedalo> tempSeatList = new List<RezerviranoSjedalo>();
            foreach (var seatHelper in seatHelperList)
            {
                RezerviranoSjedalo reservedSeat = new RezerviranoSjedalo();

                reservedSeat.rezervacijaId = reservation.rezervacijaId;
                reservedSeat.sjedaloId = seatHelper.seatId;

                tempSeatList.Add(reservedSeat);
            }
            return tempSeatList;
        }

        public static Rezervacija saveReservation(Predstava show, string name, string surename, string telephone, bool buy = false)
        {
            using (var db = new KinoDBContext())
            {
                Rezervacija reservation = new Rezervacija();

                reservation.ime = name;
                reservation.prezime = surename;
                reservation.telefon = telephone;
                reservation.predstavaId = show.predstavaId;
                reservation.kupljeno = buy;

                db.Rezervacije.Add(reservation);
                db.SaveChanges();

                return reservation;
            }
        }

        public static void saveSeats(List<SjedaloHelper> seatHelperList, Rezervacija reservation, int numberOfReservedSeats)
        {
            List<RezerviranoSjedalo> reservedSeatList = new List<RezerviranoSjedalo>();
            reservedSeatList = returnSeatList(seatHelperList, reservation);
            using (var db = new KinoDBContext())
            {
                foreach (var seat in reservedSeatList)
                {
                    db.RezerviranaSjedala.Add(seat);
                }

                reservation = db.Rezervacije.Find(reservation.rezervacijaId);

                decimal priceOfSeat = reservation.Predstava.TipPredstave.cijenaPredstave;
                reservation.cijena = numberOfReservedSeats * priceOfSeat;

                db.Entry(reservation).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public static Rezervacija getReservation(Predstava show, SjedaloHelper seatHelper)
        {
            using (var db = new KinoDBContext())
            {
                var query = from Rezervacija in db.Rezervacije
                            join Predstava in db.Predstave on Rezervacija.predstavaId equals Predstava.predstavaId
                            join RezerviranoSjedalo in db.RezerviranaSjedala on Rezervacija.rezervacijaId equals RezerviranoSjedalo.rezervacijaId
                            where Predstava.predstavaId == show.predstavaId && RezerviranoSjedalo.sjedaloId == seatHelper.seatId
                            select Rezervacija;

                return query.FirstOrDefault();
            }
        }

        public static void deleteReservation(Rezervacija reservation)
        {
            using (var db = new KinoDBContext())
            {
                reservation = db.Rezervacije.Find(reservation.rezervacijaId);
                foreach (var reservedSeat in reservation.RezerviranaSjedala.ToList())
                {
                    db.RezerviranaSjedala.Remove(reservedSeat);
                }
                db.Rezervacije.Remove(reservation);
                db.SaveChanges();
            }
        }
    }
}
