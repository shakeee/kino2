﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiNO2.Logic
{
    static partial class KarteLogic
    {
        public static List<Rezervacija> TraziBrojRezervacije(string ime, string prezime)
        {
            List<Rezervacija> listaRezervacije = new List<Rezervacija>();
            using (var db = new KinoDBContext())
            {
                listaRezervacije = db.Rezervacije.Where(r => r.ime == ime && r.prezime == prezime && r.kupljeno == false).ToList();
                return listaRezervacije;
            }
        }

        public static List<Rezervacija> TraziBrojRezervacije(int brojRezervacije)
        {
            List<Rezervacija> listaRezervacije = new List<Rezervacija>();
            using (var db = new KinoDBContext())
            {
                listaRezervacije.Add(db.Rezervacije.Find(brojRezervacije));
                return listaRezervacije;
            }
        }

        public static void KupiRezervaciju(Rezervacija rezervacija)
        {
            using (var db = new KinoDBContext())
            {
                db.Rezervacije.Attach(rezervacija);
                rezervacija.kupljeno = true;
                db.Entry(rezervacija).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
