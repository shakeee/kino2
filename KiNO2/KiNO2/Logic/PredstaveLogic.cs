﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KiNO2.Entities;
using KiNO2.Logic;

namespace KiNO2.Logic
{
    static partial class PredstaveLogic
    {
        public static List<OpisPredstava> DohvatPredstava(DateTime datum)
        {
            List<OpisPredstava> opredd = new List<OpisPredstava>();
            using (var db = new KinoDBContext())
            {
                foreach (Predstava p in db.Predstave)
                {
                    if (p.datum == datum.Date)
                    {
                        opredd.Add(new OpisPredstava(p.predstavaId, p.Film.naziv, p.datum, p.vrijeme, p.Dvorana.naziv));
                    }
                }
            }
            return opredd;
        }

        public static BindingList<Film> PrikaziFIlmove()
        {
            BindingList<Film> listaFilmova = null;
            using (var db = new KinoDBContext())
            {
                listaFilmova = new BindingList<Film>(db.Filmovi.ToList());
            }
            return listaFilmova;
        }

        public static BindingList<Zaposlenik> PrikaziZaposlenike()
        {
            BindingList<Zaposlenik> listaZaposlenika = null;
            using (var db = new KinoDBContext())
            {
                listaZaposlenika = new BindingList<Zaposlenik>(db.Zaposlenici.ToList());
            }
            return listaZaposlenika;
        }

        public static BindingList<Dvorana> PrikaziDvorane()
        {
            BindingList<Dvorana> listaDvorana = null;
            using (var db = new KinoDBContext())
            {
                listaDvorana = new BindingList<Dvorana>(db.Dvorane.ToList());
            }
            return listaDvorana;
        }

        public static BindingList<TipPredstave> PrikaziTipPredstave()
        {
            BindingList<TipPredstave> tipPredstave = null;
            using (var db = new KinoDBContext())
            {
                tipPredstave = new BindingList<TipPredstave>(db.TipoviPredstava.ToList());
            }
            return tipPredstave;
        }

        public static void SpremiPredstavu(int filmid, int dvoranaid, int zaposlenikid, DateTime datum, TimeSpan vrijeme, int tpredstavaid)
        {
            Predstava predd = new Predstava();
            predd.filmId = filmid;
            predd.dvoranaId = dvoranaid;
            predd.zaposlenikId = zaposlenikid;
            predd.datum = datum;
            predd.vrijeme = vrijeme;
            predd.tipPredstaveId = tpredstavaid;
            using (var db = new KinoDBContext())
            {
                db.Predstave.Add(predd);
                db.SaveChanges();
            }
        }


        public static void UkloniPredstavu(Predstava predstava)
        {
            using(var db = new KinoDBContext())
            {
                foreach (var rezervacija in predstava.Rezervacije)
                {
                    for (int i = 0; i < rezervacija.RezerviranaSjedala.Count(); i++)
                    {
                        db.Entry(rezervacija.RezerviranaSjedala.ToList()[0]).State = System.Data.Entity.EntityState.Deleted;
                    }
                }

                for (int i = 0; i < predstava.Rezervacije.Count(); i++)
                {
                    db.Entry(predstava.Rezervacije.ToList()[0]).State = System.Data.Entity.EntityState.Deleted;
                }
            }
        }
    }
}
