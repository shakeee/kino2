﻿using KiNO2.HelperClasses;
using KiNO2.Properties;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KiNO2.Logic
{
    static partial class DvoraneLogic
    {
        public static List<Dvorana> getTheatherRoomsList()
        {
            List<Dvorana> theatherRoomList = new List<Dvorana>();
            using (var db = new KinoDBContext())
            {
                return theatherRoomList = db.Dvorane.ToList();
            }
        }

        public static List<Sjedalo> getSeatsList(Dvorana theatherRoom)
        {
            using (var db = new KinoDBContext())
            {
                db.Dvorane.Attach(theatherRoom);
                return theatherRoom.Sjedala.ToList();
            }
        }

        public static List<SjedaloHelper> returnSeatHelperList(List<Sjedalo> seatList)
        {
            List<SjedaloHelper> tempSeatHelperList = new List<SjedaloHelper>();
            foreach (var seat in seatList)
            {
                SjedaloHelper seatHelper = new SjedaloHelper();

                seatHelper.seatId = seat.sjedaloId;
                seatHelper.seatPositionSet = true;
                seatHelper.position = new Point(seat.pozicijaX, seat.pozicijaY);
                seatHelper.seatTypeId = seat.tipSjedalaId;

                seatHelper.image = new PictureBox();
                seatHelper.image.SizeMode = PictureBoxSizeMode.AutoSize;

                if (seatHelper.seatTypeId == 1)
                {
                    seatHelper.image.Image = Resources.s;
                }
                else if (seatHelper.seatTypeId == 2)
                {
                    seatHelper.image.Image = Resources.l;
                }

                tempSeatHelperList.Add(seatHelper);
            }

            return tempSeatHelperList;
        }

        public static List<Sjedalo> returnSeatList(List<SjedaloHelper> seatHelperList, Dvorana theatherRoom)
        {
            List<Sjedalo> tempSeatList = new List<Sjedalo>();
            foreach (var seatHelper in seatHelperList)
            {
                Sjedalo seat = new Sjedalo();
                                
                seat.dvoranaId = theatherRoom.dvoranaId;
                seat.sjedaloId = seatHelper.seatId;
                seat.pozicijaX = seatHelper.position.X;
                seat.pozicijaY = seatHelper.position.Y;
                seat.tipSjedalaId = seatHelper.seatTypeId;

                tempSeatList.Add(seat);
            }

            return tempSeatList;
        }

        public static Dvorana saveTheatherRoom(Dvorana theatherRoom)
        {
            using (var db = new KinoDBContext())
            {
                Dvorana newTheatherRoom = new Dvorana();

                if (theatherRoom.dvoranaId == 0)
                {
                    newTheatherRoom = theatherRoom;
                    newTheatherRoom.naziv = "TempNaziv";
                    db.Dvorane.Add(newTheatherRoom);
                }
                else
                {
                    db.Entry(theatherRoom).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
                return theatherRoom;
            }
        }

        public static void saveSeats(List<SjedaloHelper> seatHelperList, Dvorana theatherRoom)
        {
            List<Sjedalo> seatList = new List<Sjedalo>();
            seatList = returnSeatList(seatHelperList, theatherRoom);
            using (var db = new KinoDBContext())
            {
                //theatherRoom.Sjedala.Clear();
                foreach (var seat in theatherRoom.Sjedala.ToList())
                {
                    if (db.Sjedala.Any(s => s.sjedaloId == seat.sjedaloId))
                    {
                        db.Entry(seat).State = EntityState.Deleted;
                    }
                }

                db.SaveChanges();

                foreach (var seat in seatList)
                {
                    db.Sjedala.Add(seat);
                }

                db.SaveChanges();
            }
        }

        public static void deleteUnsavedChanges(Dvorana theatherRoom)
        {
            if (theatherRoom.naziv == "TempNaziv")
            {
                using (var db = new KinoDBContext())
                {
                    db.Dvorane.Attach(theatherRoom);
                    db.Dvorane.Remove(theatherRoom);
                    db.SaveChanges();
                }
            }
        }

        public static void deleteTheaterRoom(Dvorana theatherRoom)
        {
            using (var db = new KinoDBContext())
            {
                foreach (var seat in theatherRoom.Sjedala.ToList())
                {
                    if (db.Sjedala.Any(s => s.sjedaloId == seat.sjedaloId))
                    {
                        db.Entry(seat).State = EntityState.Deleted;
                    }
                }

                db.Entry(theatherRoom).State = EntityState.Deleted;

                db.SaveChanges();
            }

        }
    }
}
