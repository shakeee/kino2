﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KiNO2;

namespace KiNO2.Logic
{
     public class OsobljeLogic
    {
        /// <summary>
        /// Method for inserting new records in the database.
        /// </summary>
        /// <param name="Ime"></param>
        /// <param name="Prezime"></param>
        /// <param name="Email"></param>
        /// <param name="Lozinka"></param>
        /// <param name="Kontakt"></param>
        /// <param name="Kategorija"></param>
        public static void UnosNovogKorisnika(string Ime, string Prezime, string Email, string Lozinka, string Kontakt, int Kategorija)
        {
            Zaposlenik zap = new Zaposlenik();
            using (var db = new KinoDBContext())
            {
                zap.ime = Ime;
                zap.prezime = Prezime;
                zap.email = Email;
                zap.lozinka = Lozinka;
                zap.telefon = Kontakt;
                zap.kategorijaZaposlenikaId = Kategorija;
                db.Zaposlenici.Add(zap);
                db.SaveChanges();
            }
        }
        /// <summary>
        /// Method for updating existing records in the database.
        /// </summary>
        /// <param name="zaposlenik"></param>
        public static void AzuriranjePostojecegKorisnika(Zaposlenik zaposlenik)
        {
            Zaposlenik zap;
            using (var db = new KinoDBContext())
            {
                zap = db.Zaposlenici.Where(z => z.zaposlenikId == zaposlenik.zaposlenikId).FirstOrDefault<Zaposlenik>();

                if(zap != null)
                {
                    zap.ime = zaposlenik.ime;
                    zap.prezime = zaposlenik.prezime;
                    zap.email = zaposlenik.email;
                    zap.lozinka = zaposlenik.lozinka;
                    zap.telefon = zaposlenik.telefon;
                    zap.kategorijaZaposlenikaId = zaposlenik.kategorijaZaposlenikaId;
                }
            }
            using (var newdb = new KinoDBContext())
            {
                newdb.Entry(zap).State = System.Data.Entity.EntityState.Modified;
                newdb.SaveChanges();
            }
        }
    }
}
