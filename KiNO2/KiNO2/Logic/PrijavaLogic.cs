﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiNO2.Logic
{
    static partial class PrijavaLogic
    {
        /// <summary>
        /// Metoda za autentifikaciju korisnika prilikom prijave u aplikaciju
        /// </summary>
        /// <param name="email"></param>
        /// <param name="lozinka"></param>
        /// <returns></returns>
        public static int autentifikacijaKorisnika(string korisnickoIme, string lozinka)
        {
            using (var db = new KinoDBContext())
            {
                var query = from zaposlenici in db.Zaposlenici orderby zaposlenici.zaposlenikId select zaposlenici;

                foreach (var zaposlenik in query.ToList<Zaposlenik>())
                {
                    if (zaposlenik.email == korisnickoIme)
                    {
                        if (zaposlenik.lozinka == lozinka)
                        {
                            return zaposlenik.kategorijaZaposlenikaId;
                        }
                    }
                }
                return 0;
            }
        }

    }
}