﻿namespace KiNO2
{
    partial class UnosFilma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBoxTrazenje = new System.Windows.Forms.TextBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.AddButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Zanr = new System.Windows.Forms.TextBox();
            this.Opis = new System.Windows.Forms.TextBox();
            this.Naslov = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBoxTrazenje
            // 
            this.txtBoxTrazenje.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtBoxTrazenje.Location = new System.Drawing.Point(12, 12);
            this.txtBoxTrazenje.Name = "txtBoxTrazenje";
            this.txtBoxTrazenje.Size = new System.Drawing.Size(180, 20);
            this.txtBoxTrazenje.TabIndex = 0;
            // 
            // SearchButton
            // 
            this.SearchButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.SearchButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SearchButton.FlatAppearance.BorderSize = 0;
            this.SearchButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.SearchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchButton.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.SearchButton.Location = new System.Drawing.Point(12, 51);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(147, 37);
            this.SearchButton.TabIndex = 1;
            this.SearchButton.Text = "Traži";
            this.SearchButton.UseVisualStyleBackColor = false;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // listBox1
            // 
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 97);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(180, 182);
            this.listBox1.TabIndex = 2;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // AddButton
            // 
            this.AddButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.AddButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.AddButton.FlatAppearance.BorderSize = 0;
            this.AddButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.AddButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddButton.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.AddButton.Location = new System.Drawing.Point(12, 299);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(147, 37);
            this.AddButton.TabIndex = 3;
            this.AddButton.Text = "Dodaj";
            this.AddButton.UseVisualStyleBackColor = false;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.Zanr);
            this.panel1.Controls.Add(this.Opis);
            this.panel1.Controls.Add(this.Naslov);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(209, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(501, 351);
            this.panel1.TabIndex = 4;
            // 
            // Zanr
            // 
            this.Zanr.BackColor = System.Drawing.Color.White;
            this.Zanr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Zanr.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Zanr.Location = new System.Drawing.Point(226, 54);
            this.Zanr.Multiline = true;
            this.Zanr.Name = "Zanr";
            this.Zanr.Size = new System.Drawing.Size(272, 37);
            this.Zanr.TabIndex = 3;
            this.Zanr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Opis
            // 
            this.Opis.BackColor = System.Drawing.Color.White;
            this.Opis.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Opis.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Opis.Location = new System.Drawing.Point(226, 97);
            this.Opis.Multiline = true;
            this.Opis.Name = "Opis";
            this.Opis.Size = new System.Drawing.Size(272, 251);
            this.Opis.TabIndex = 2;
            this.Opis.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Naslov
            // 
            this.Naslov.BackColor = System.Drawing.Color.White;
            this.Naslov.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Naslov.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Naslov.Location = new System.Drawing.Point(226, 4);
            this.Naslov.Multiline = true;
            this.Naslov.Name = "Naslov";
            this.Naslov.Size = new System.Drawing.Size(272, 43);
            this.Naslov.TabIndex = 1;
            this.Naslov.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(220, 303);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // UnosFilma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(722, 375);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.txtBoxTrazenje);
            this.KeyPreview = true;
            this.Name = "UnosFilma";
            this.Text = "UnosFilma";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UnosFilma_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBoxTrazenje;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        //private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Naslov;
        private System.Windows.Forms.TextBox Opis;
        private System.Windows.Forms.TextBox Zanr;
    }
}