namespace KiNO2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dvorana",
                c => new
                    {
                        dvoranaId = c.Int(nullable: false, identity: true),
                        naziv = c.String(nullable: false, maxLength: 10),
                        sirina = c.Int(nullable: false),
                        duljina = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.dvoranaId);
            
            CreateTable(
                "dbo.Predstava",
                c => new
                    {
                        predstavaId = c.Int(nullable: false, identity: true),
                        tipPredstaveId = c.Int(nullable: false),
                        dvoranaId = c.Int(nullable: false),
                        filmId = c.Int(nullable: false),
                        datum = c.DateTime(nullable: false, storeType: "date"),
                        vrijeme = c.Time(nullable: false, precision: 7),
                        zaposlenikId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.predstavaId)
                .ForeignKey("dbo.Dvorana", t => t.dvoranaId, cascadeDelete: true)
                .ForeignKey("dbo.Film", t => t.filmId, cascadeDelete: true)
                .ForeignKey("dbo.TipPredstave", t => t.tipPredstaveId, cascadeDelete: true)
                .ForeignKey("dbo.Zaposlenik", t => t.zaposlenikId, cascadeDelete: true)
                .Index(t => t.tipPredstaveId)
                .Index(t => t.dvoranaId)
                .Index(t => t.filmId)
                .Index(t => t.zaposlenikId);
            
            CreateTable(
                "dbo.Film",
                c => new
                    {
                        filmId = c.Int(nullable: false, identity: true),
                        zanrId = c.String(nullable: false, maxLength: 50),
                        tmdbID = c.Int(nullable: false),
                        naziv = c.String(nullable: false, maxLength: 100),
                        opis = c.String(nullable: false, maxLength: 1000),
                        slikaUri = c.String(nullable: false, maxLength: 1000),
                    })
                .PrimaryKey(t => t.filmId);
            
            CreateTable(
                "dbo.Rezervacija",
                c => new
                    {
                        rezervacijaId = c.Int(nullable: false, identity: true),
                        predstavaId = c.Int(nullable: false),
                        ime = c.String(maxLength: 30),
                        prezime = c.String(maxLength: 30),
                        telefon = c.String(maxLength: 30),
                        cijena = c.Decimal(nullable: false, storeType: "money"),
                        kupljeno = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.rezervacijaId)
                .ForeignKey("dbo.Predstava", t => t.predstavaId, cascadeDelete: true)
                .Index(t => t.predstavaId);
            
            CreateTable(
                "dbo.RezerviranoSjedalo",
                c => new
                    {
                        rezerviranoSjedaloId = c.Int(nullable: false, identity: true),
                        sjedaloId = c.Int(nullable: false),
                        rezervacijaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.rezerviranoSjedaloId)
                .ForeignKey("dbo.Sjedalo", t => t.sjedaloId, cascadeDelete: true)
                .ForeignKey("dbo.Rezervacija", t => t.rezervacijaId)
                .Index(t => t.sjedaloId)
                .Index(t => t.rezervacijaId);
            
            CreateTable(
                "dbo.Sjedalo",
                c => new
                    {
                        sjedaloId = c.Int(nullable: false, identity: true),
                        tipSjedalaId = c.Int(nullable: false),
                        dvoranaId = c.Int(nullable: false),
                        pozicijaX = c.Int(nullable: false),
                        pozicijaY = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.sjedaloId)
                .ForeignKey("dbo.Dvorana", t => t.dvoranaId, cascadeDelete: true)
                .ForeignKey("dbo.TipSjedala", t => t.tipSjedalaId, cascadeDelete: true)
                .Index(t => t.tipSjedalaId)
                .Index(t => t.dvoranaId);
            
            CreateTable(
                "dbo.TipSjedala",
                c => new
                    {
                        tipSjedalaId = c.Int(nullable: false, identity: true),
                        velicina = c.Int(nullable: false),
                        naziv = c.String(nullable: false, maxLength: 15),
                    })
                .PrimaryKey(t => t.tipSjedalaId);
            
            CreateTable(
                "dbo.TipPredstave",
                c => new
                    {
                        tipPredstaveId = c.Int(nullable: false, identity: true),
                        cijenaPredstave = c.Decimal(nullable: false, storeType: "money"),
                        naziv = c.String(nullable: false, maxLength: 15),
                    })
                .PrimaryKey(t => t.tipPredstaveId);
            
            CreateTable(
                "dbo.Zaposlenik",
                c => new
                    {
                        zaposlenikId = c.Int(nullable: false, identity: true),
                        ime = c.String(nullable: false, maxLength: 30),
                        prezime = c.String(nullable: false, maxLength: 30),
                        email = c.String(nullable: false, maxLength: 50),
                        lozinka = c.String(nullable: false, maxLength: 64, fixedLength: true),
                        telefon = c.String(nullable: false, maxLength: 15),
                        kategorijaZaposlenikaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.zaposlenikId)
                .ForeignKey("dbo.KategorijaZaposlenika", t => t.kategorijaZaposlenikaId, cascadeDelete: true)
                .Index(t => t.kategorijaZaposlenikaId);
            
            CreateTable(
                "dbo.KategorijaZaposlenika",
                c => new
                    {
                        kategorijaZaposlenikaId = c.Int(nullable: false, identity: true),
                        naziv = c.String(nullable: false, maxLength: 50),
                        opis = c.String(nullable: false, maxLength: 1000),
                    })
                .PrimaryKey(t => t.kategorijaZaposlenikaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Predstava", "zaposlenikId", "dbo.Zaposlenik");
            DropForeignKey("dbo.Zaposlenik", "kategorijaZaposlenikaId", "dbo.KategorijaZaposlenika");
            DropForeignKey("dbo.Predstava", "tipPredstaveId", "dbo.TipPredstave");
            DropForeignKey("dbo.RezerviranoSjedalo", "rezervacijaId", "dbo.Rezervacija");
            DropForeignKey("dbo.Sjedalo", "tipSjedalaId", "dbo.TipSjedala");
            DropForeignKey("dbo.RezerviranoSjedalo", "sjedaloId", "dbo.Sjedalo");
            DropForeignKey("dbo.Sjedalo", "dvoranaId", "dbo.Dvorana");
            DropForeignKey("dbo.Rezervacija", "predstavaId", "dbo.Predstava");
            DropForeignKey("dbo.Predstava", "filmId", "dbo.Film");
            DropForeignKey("dbo.Predstava", "dvoranaId", "dbo.Dvorana");
            DropIndex("dbo.Zaposlenik", new[] { "kategorijaZaposlenikaId" });
            DropIndex("dbo.Sjedalo", new[] { "dvoranaId" });
            DropIndex("dbo.Sjedalo", new[] { "tipSjedalaId" });
            DropIndex("dbo.RezerviranoSjedalo", new[] { "rezervacijaId" });
            DropIndex("dbo.RezerviranoSjedalo", new[] { "sjedaloId" });
            DropIndex("dbo.Rezervacija", new[] { "predstavaId" });
            DropIndex("dbo.Predstava", new[] { "zaposlenikId" });
            DropIndex("dbo.Predstava", new[] { "filmId" });
            DropIndex("dbo.Predstava", new[] { "dvoranaId" });
            DropIndex("dbo.Predstava", new[] { "tipPredstaveId" });
            DropTable("dbo.KategorijaZaposlenika");
            DropTable("dbo.Zaposlenik");
            DropTable("dbo.TipPredstave");
            DropTable("dbo.TipSjedala");
            DropTable("dbo.Sjedalo");
            DropTable("dbo.RezerviranoSjedalo");
            DropTable("dbo.Rezervacija");
            DropTable("dbo.Film");
            DropTable("dbo.Predstava");
            DropTable("dbo.Dvorana");
        }
    }
}
