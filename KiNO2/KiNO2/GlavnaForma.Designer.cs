﻿using System.Windows.Forms;
namespace KiNO2
{
    partial class frmGlavnaForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGlavnaForma));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabFilmovi = new System.Windows.Forms.TabPage();
            this.pnlMainMoviePanel = new System.Windows.Forms.Panel();
            this.movieTableLayoutPanel1 = new KiNO2.View.MovieTableLayoutPanel();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnUkloniFilmIzBaze = new System.Windows.Forms.Button();
            this.btnDodajNoviFilm = new System.Windows.Forms.Button();
            this.tabDvorane = new System.Windows.Forms.TabPage();
            this.pnlTheaterRoom = new System.Windows.Forms.Panel();
            this.pnlTheaterRoomEditor = new System.Windows.Forms.Panel();
            this.lbxTheaterRooms = new System.Windows.Forms.ListBox();
            this.pnlBottomPanelTheaterRooms = new System.Windows.Forms.Panel();
            this.btnDeleteTheaterRoom = new System.Windows.Forms.Button();
            this.btnEditTheaterRoom = new System.Windows.Forms.Button();
            this.btnAddNewTheatherRoom = new System.Windows.Forms.Button();
            this.tabPredstave = new System.Windows.Forms.TabPage();
            this.btnUkloniPredstavu = new System.Windows.Forms.Button();
            this.predDatumVrijeme = new System.Windows.Forms.DateTimePicker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.predstavaIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filmDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vrijemeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvoranaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipPredstaveIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvoranaIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filmIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zaposlenikIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipPredstaveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zaposlenikDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rezervacijeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.predstavaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnNovaPredstava = new System.Windows.Forms.Button();
            this.tabRezervacije = new System.Windows.Forms.TabPage();
            this.pnlTheaterRoomReservations = new System.Windows.Forms.Panel();
            this.pnlTheaterRoomEditorReservations = new System.Windows.Forms.Panel();
            this.pnlReservations = new System.Windows.Forms.Panel();
            this.lblNumberOfTickets = new System.Windows.Forms.Label();
            this.nudNumberOfTickets = new System.Windows.Forms.NumericUpDown();
            this.lblTelephone = new System.Windows.Forms.Label();
            this.tbxTelephone = new System.Windows.Forms.TextBox();
            this.lblSurename = new System.Windows.Forms.Label();
            this.tbxSurename = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.tbxName = new System.Windows.Forms.TextBox();
            this.cbxTiming = new System.Windows.Forms.ComboBox();
            this.lblTiming = new System.Windows.Forms.Label();
            this.cbxMovies = new System.Windows.Forms.ComboBox();
            this.lblMovies = new System.Windows.Forms.Label();
            this.calReservations = new System.Windows.Forms.MonthCalendar();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnBuyTickets = new System.Windows.Forms.Button();
            this.btnReservationsList = new System.Windows.Forms.Button();
            this.btnMakeReservation = new System.Windows.Forms.Button();
            this.tabOsoblje = new System.Windows.Forms.TabPage();
            this.lblZaposlenici = new System.Windows.Forms.Label();
            this.lblKategorijeZaposlenika = new System.Windows.Forms.Label();
            this.btnUkloniKorisnika = new System.Windows.Forms.Button();
            this.btnAzuriraj = new System.Windows.Forms.Button();
            this.dgvKategorijaZaposlenika = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kategorijaZaposlenikaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._16058_DBDataSet = new KiNO2._16058_DBDataSet();
            this.dgvZaposlenici = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zaposlenikBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.btnDodajKorisnika = new System.Windows.Forms.Button();
            this.tabKarte = new System.Windows.Forms.TabPage();
            this.lblBrojRezervacije = new System.Windows.Forms.Label();
            this.txtBrojRezervacije = new System.Windows.Forms.TextBox();
            this.btnKupi = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.lblNaruciteljPrezime = new System.Windows.Forms.Label();
            this.lblNaruciteljIme = new System.Windows.Forms.Label();
            this.txtNaruciteljPrezime = new System.Windows.Forms.TextBox();
            this.txtNaruciteljIme = new System.Windows.Forms.TextBox();
            this.btnTrazi = new System.Windows.Forms.Button();
            this.tabPomoc = new System.Windows.Forms.TabPage();
            this.zaposlenikTableAdapter = new KiNO2._16058_DBDataSetTableAdapters.ZaposlenikTableAdapter();
            this.kategorijaZaposlenikaTableAdapter = new KiNO2._16058_DBDataSetTableAdapters.KategorijaZaposlenikaTableAdapter();
            this.pnlLoginPanel = new System.Windows.Forms.Panel();
            this.lblPrijava = new System.Windows.Forms.Label();
            this.lblLozinka = new System.Windows.Forms.Label();
            this.lblMail = new System.Windows.Forms.Label();
            this.txtLozinka = new System.Windows.Forms.TextBox();
            this.txtKorisnickoIme = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.tabControl.SuspendLayout();
            this.tabFilmovi.SuspendLayout();
            this.pnlMainMoviePanel.SuspendLayout();
            this.tabDvorane.SuspendLayout();
            this.pnlTheaterRoom.SuspendLayout();
            this.pnlBottomPanelTheaterRooms.SuspendLayout();
            this.tabPredstave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.predstavaBindingSource)).BeginInit();
            this.tabRezervacije.SuspendLayout();
            this.pnlTheaterRoomReservations.SuspendLayout();
            this.pnlReservations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfTickets)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabOsoblje.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKategorijaZaposlenika)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kategorijaZaposlenikaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._16058_DBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvZaposlenici)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zaposlenikBindingSource1)).BeginInit();
            this.tabKarte.SuspendLayout();
            this.pnlLoginPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabFilmovi);
            this.tabControl.Controls.Add(this.tabDvorane);
            this.tabControl.Controls.Add(this.tabPredstave);
            this.tabControl.Controls.Add(this.tabRezervacije);
            this.tabControl.Controls.Add(this.tabOsoblje);
            this.tabControl.Controls.Add(this.tabKarte);
            this.tabControl.Controls.Add(this.tabPomoc);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.HotTrack = true;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(970, 605);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabFilmovi
            // 
            this.tabFilmovi.Controls.Add(this.pnlMainMoviePanel);
            this.tabFilmovi.Controls.Add(this.button12);
            this.tabFilmovi.Controls.Add(this.button11);
            this.tabFilmovi.Controls.Add(this.button10);
            this.tabFilmovi.Controls.Add(this.button9);
            this.tabFilmovi.Controls.Add(this.button8);
            this.tabFilmovi.Controls.Add(this.button7);
            this.tabFilmovi.Controls.Add(this.button6);
            this.tabFilmovi.Controls.Add(this.button5);
            this.tabFilmovi.Controls.Add(this.button4);
            this.tabFilmovi.Controls.Add(this.btnUkloniFilmIzBaze);
            this.tabFilmovi.Controls.Add(this.btnDodajNoviFilm);
            this.tabFilmovi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabFilmovi.Location = new System.Drawing.Point(4, 29);
            this.tabFilmovi.Name = "tabFilmovi";
            this.tabFilmovi.Padding = new System.Windows.Forms.Padding(3);
            this.tabFilmovi.Size = new System.Drawing.Size(962, 572);
            this.tabFilmovi.TabIndex = 0;
            this.tabFilmovi.Text = "Filmovi";
            this.tabFilmovi.UseVisualStyleBackColor = true;
            // 
            // pnlMainMoviePanel
            // 
            this.pnlMainMoviePanel.AutoScroll = true;
            this.pnlMainMoviePanel.Controls.Add(this.movieTableLayoutPanel1);
            this.pnlMainMoviePanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlMainMoviePanel.Location = new System.Drawing.Point(157, 3);
            this.pnlMainMoviePanel.Name = "pnlMainMoviePanel";
            this.pnlMainMoviePanel.Size = new System.Drawing.Size(802, 566);
            this.pnlMainMoviePanel.TabIndex = 7;
            // 
            // movieTableLayoutPanel1
            // 
            this.movieTableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.movieTableLayoutPanel1.ColumnCount = 3;
            this.movieTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.movieTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 284F));
            this.movieTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 285F));
            this.movieTableLayoutPanel1.Location = new System.Drawing.Point(5, 3);
            this.movieTableLayoutPanel1.Name = "movieTableLayoutPanel1";
            this.movieTableLayoutPanel1.RowCount = 3;
            this.movieTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.movieTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.movieTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 172F));
            this.movieTableLayoutPanel1.Size = new System.Drawing.Size(775, 543);
            this.movieTableLayoutPanel1.TabIndex = 1;
            this.movieTableLayoutPanel1.visinaOmjer = 1.5F;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button12.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.button12.Location = new System.Drawing.Point(4, 318);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(147, 37);
            this.button12.TabIndex = 6;
            this.button12.Tag = "Western";
            this.button12.Text = "Vestern";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button4_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button11.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.button11.Location = new System.Drawing.Point(4, 282);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(147, 37);
            this.button11.TabIndex = 6;
            this.button11.Tag = "Fantasy";
            this.button11.Text = "Fantazija";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button4_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button10.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.button10.Location = new System.Drawing.Point(4, 246);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(147, 37);
            this.button10.TabIndex = 6;
            this.button10.Tag = "Horror";
            this.button10.Text = "Horor";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button4_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.button9.Location = new System.Drawing.Point(4, 210);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(147, 37);
            this.button9.TabIndex = 6;
            this.button9.Tag = "Crime";
            this.button9.Text = "Kriminalistički";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button4_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.button8.Location = new System.Drawing.Point(4, 174);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(147, 37);
            this.button8.TabIndex = 6;
            this.button8.Tag = "Animation";
            this.button8.Text = "Animirani";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button4_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.button7.Location = new System.Drawing.Point(4, 138);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(147, 37);
            this.button7.TabIndex = 6;
            this.button7.Tag = "Science Fiction";
            this.button7.Text = "Znanstvena fantastika";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button4_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.button6.Location = new System.Drawing.Point(4, 102);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(147, 37);
            this.button6.TabIndex = 6;
            this.button6.Tag = "Drama";
            this.button6.Text = "Drama";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.button5.Location = new System.Drawing.Point(4, 66);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(147, 37);
            this.button5.TabIndex = 6;
            this.button5.Tag = "Comedy";
            this.button5.Text = "Komedija";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button4_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.button4.Location = new System.Drawing.Point(4, 30);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(147, 37);
            this.button4.TabIndex = 6;
            this.button4.Tag = "Action";
            this.button4.Text = "Akcija";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnUkloniFilmIzBaze
            // 
            this.btnUkloniFilmIzBaze.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUkloniFilmIzBaze.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnUkloniFilmIzBaze.FlatAppearance.BorderSize = 0;
            this.btnUkloniFilmIzBaze.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.btnUkloniFilmIzBaze.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUkloniFilmIzBaze.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnUkloniFilmIzBaze.Location = new System.Drawing.Point(4, 458);
            this.btnUkloniFilmIzBaze.Name = "btnUkloniFilmIzBaze";
            this.btnUkloniFilmIzBaze.Size = new System.Drawing.Size(147, 37);
            this.btnUkloniFilmIzBaze.TabIndex = 5;
            this.btnUkloniFilmIzBaze.Text = "Ukloni Film";
            this.btnUkloniFilmIzBaze.UseVisualStyleBackColor = false;
            this.btnUkloniFilmIzBaze.Click += new System.EventHandler(this.btnUkloniFilmIzBaze_Click);
            // 
            // btnDodajNoviFilm
            // 
            this.btnDodajNoviFilm.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDodajNoviFilm.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnDodajNoviFilm.FlatAppearance.BorderSize = 0;
            this.btnDodajNoviFilm.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.btnDodajNoviFilm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodajNoviFilm.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnDodajNoviFilm.Location = new System.Drawing.Point(4, 422);
            this.btnDodajNoviFilm.Name = "btnDodajNoviFilm";
            this.btnDodajNoviFilm.Size = new System.Drawing.Size(147, 37);
            this.btnDodajNoviFilm.TabIndex = 4;
            this.btnDodajNoviFilm.Text = "Dodaj Novi Film";
            this.btnDodajNoviFilm.UseVisualStyleBackColor = false;
            this.btnDodajNoviFilm.Click += new System.EventHandler(this.btnDodajNoviFilm_Click);
            // 
            // tabDvorane
            // 
            this.tabDvorane.Controls.Add(this.pnlTheaterRoom);
            this.tabDvorane.Controls.Add(this.lbxTheaterRooms);
            this.tabDvorane.Controls.Add(this.pnlBottomPanelTheaterRooms);
            this.tabDvorane.Location = new System.Drawing.Point(4, 29);
            this.tabDvorane.Name = "tabDvorane";
            this.tabDvorane.Padding = new System.Windows.Forms.Padding(3);
            this.tabDvorane.Size = new System.Drawing.Size(962, 572);
            this.tabDvorane.TabIndex = 1;
            this.tabDvorane.Text = "Dvorane";
            this.tabDvorane.UseVisualStyleBackColor = true;
            // 
            // pnlTheaterRoom
            // 
            this.pnlTheaterRoom.BackColor = System.Drawing.Color.IndianRed;
            this.pnlTheaterRoom.BackgroundImage = global::KiNO2.Properties.Resources.screen;
            this.pnlTheaterRoom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlTheaterRoom.Controls.Add(this.pnlTheaterRoomEditor);
            this.pnlTheaterRoom.Location = new System.Drawing.Point(348, 40);
            this.pnlTheaterRoom.Name = "pnlTheaterRoom";
            this.pnlTheaterRoom.Size = new System.Drawing.Size(452, 457);
            this.pnlTheaterRoom.TabIndex = 8;
            // 
            // pnlTheaterRoomEditor
            // 
            this.pnlTheaterRoomEditor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlTheaterRoomEditor.BackColor = System.Drawing.Color.Transparent;
            this.pnlTheaterRoomEditor.Location = new System.Drawing.Point(138, 160);
            this.pnlTheaterRoomEditor.Name = "pnlTheaterRoomEditor";
            this.pnlTheaterRoomEditor.Size = new System.Drawing.Size(172, 159);
            this.pnlTheaterRoomEditor.TabIndex = 0;
            // 
            // lbxTheaterRooms
            // 
            this.lbxTheaterRooms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbxTheaterRooms.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbxTheaterRooms.FormattingEnabled = true;
            this.lbxTheaterRooms.ItemHeight = 20;
            this.lbxTheaterRooms.Location = new System.Drawing.Point(3, 3);
            this.lbxTheaterRooms.Name = "lbxTheaterRooms";
            this.lbxTheaterRooms.Size = new System.Drawing.Size(203, 534);
            this.lbxTheaterRooms.TabIndex = 2;
            this.lbxTheaterRooms.SelectedIndexChanged += new System.EventHandler(this.lbxTheaterRooms_SelectedIndexChanged);
            // 
            // pnlBottomPanelTheaterRooms
            // 
            this.pnlBottomPanelTheaterRooms.AutoScroll = true;
            this.pnlBottomPanelTheaterRooms.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlBottomPanelTheaterRooms.Controls.Add(this.btnDeleteTheaterRoom);
            this.pnlBottomPanelTheaterRooms.Controls.Add(this.btnEditTheaterRoom);
            this.pnlBottomPanelTheaterRooms.Controls.Add(this.btnAddNewTheatherRoom);
            this.pnlBottomPanelTheaterRooms.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottomPanelTheaterRooms.Location = new System.Drawing.Point(3, 537);
            this.pnlBottomPanelTheaterRooms.Name = "pnlBottomPanelTheaterRooms";
            this.pnlBottomPanelTheaterRooms.Size = new System.Drawing.Size(956, 32);
            this.pnlBottomPanelTheaterRooms.TabIndex = 1;
            // 
            // btnDeleteTheaterRoom
            // 
            this.btnDeleteTheaterRoom.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDeleteTheaterRoom.Location = new System.Drawing.Point(728, 0);
            this.btnDeleteTheaterRoom.Name = "btnDeleteTheaterRoom";
            this.btnDeleteTheaterRoom.Size = new System.Drawing.Size(114, 32);
            this.btnDeleteTheaterRoom.TabIndex = 3;
            this.btnDeleteTheaterRoom.Text = "Obriši";
            this.btnDeleteTheaterRoom.UseVisualStyleBackColor = true;
            this.btnDeleteTheaterRoom.Click += new System.EventHandler(this.btnDeleteTheaterRoom_Click);
            // 
            // btnEditTheaterRoom
            // 
            this.btnEditTheaterRoom.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnEditTheaterRoom.Location = new System.Drawing.Point(842, 0);
            this.btnEditTheaterRoom.Name = "btnEditTheaterRoom";
            this.btnEditTheaterRoom.Size = new System.Drawing.Size(114, 32);
            this.btnEditTheaterRoom.TabIndex = 2;
            this.btnEditTheaterRoom.Text = "Uredi";
            this.btnEditTheaterRoom.UseVisualStyleBackColor = true;
            this.btnEditTheaterRoom.Click += new System.EventHandler(this.btnEditTheaterRoom_Click);
            // 
            // btnAddNewTheatherRoom
            // 
            this.btnAddNewTheatherRoom.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAddNewTheatherRoom.Location = new System.Drawing.Point(0, 0);
            this.btnAddNewTheatherRoom.Name = "btnAddNewTheatherRoom";
            this.btnAddNewTheatherRoom.Size = new System.Drawing.Size(114, 32);
            this.btnAddNewTheatherRoom.TabIndex = 0;
            this.btnAddNewTheatherRoom.Text = "Nova dvorana";
            this.btnAddNewTheatherRoom.UseVisualStyleBackColor = true;
            this.btnAddNewTheatherRoom.Click += new System.EventHandler(this.btnAddNewTheatherRoom_Click);
            // 
            // tabPredstave
            // 
            this.tabPredstave.Controls.Add(this.btnUkloniPredstavu);
            this.tabPredstave.Controls.Add(this.predDatumVrijeme);
            this.tabPredstave.Controls.Add(this.dataGridView1);
            this.tabPredstave.Controls.Add(this.btnNovaPredstava);
            this.tabPredstave.Location = new System.Drawing.Point(4, 29);
            this.tabPredstave.Name = "tabPredstave";
            this.tabPredstave.Padding = new System.Windows.Forms.Padding(3);
            this.tabPredstave.Size = new System.Drawing.Size(962, 572);
            this.tabPredstave.TabIndex = 2;
            this.tabPredstave.Text = "Predstave";
            this.tabPredstave.UseVisualStyleBackColor = true;
            // 
            // btnUkloniPredstavu
            // 
            this.btnUkloniPredstavu.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUkloniPredstavu.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnUkloniPredstavu.FlatAppearance.BorderSize = 0;
            this.btnUkloniPredstavu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUkloniPredstavu.Location = new System.Drawing.Point(23, 287);
            this.btnUkloniPredstavu.Name = "btnUkloniPredstavu";
            this.btnUkloniPredstavu.Size = new System.Drawing.Size(147, 37);
            this.btnUkloniPredstavu.TabIndex = 3;
            this.btnUkloniPredstavu.Text = "Ukloni";
            this.btnUkloniPredstavu.UseVisualStyleBackColor = false;
            this.btnUkloniPredstavu.Click += new System.EventHandler(this.btnUkloniPredstavu_Click);
            // 
            // predDatumVrijeme
            // 
            this.predDatumVrijeme.Location = new System.Drawing.Point(23, 19);
            this.predDatumVrijeme.Name = "predDatumVrijeme";
            this.predDatumVrijeme.Size = new System.Drawing.Size(147, 27);
            this.predDatumVrijeme.TabIndex = 2;
            this.predDatumVrijeme.ValueChanged += new System.EventHandler(this.predDatumVrijeme_ValueChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.OrangeRed;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.predstavaIdDataGridViewTextBoxColumn,
            this.filmDataGridViewTextBoxColumn,
            this.datumDataGridViewTextBoxColumn,
            this.vrijemeDataGridViewTextBoxColumn,
            this.dvoranaDataGridViewTextBoxColumn,
            this.tipPredstaveIdDataGridViewTextBoxColumn,
            this.dvoranaIdDataGridViewTextBoxColumn,
            this.filmIdDataGridViewTextBoxColumn,
            this.zaposlenikIdDataGridViewTextBoxColumn,
            this.tipPredstaveDataGridViewTextBoxColumn,
            this.zaposlenikDataGridViewTextBoxColumn,
            this.rezervacijeDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.predstavaBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.OrangeRed;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.GridColor = System.Drawing.Color.White;
            this.dataGridView1.Location = new System.Drawing.Point(194, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.OrangeRed;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.OrangeRed;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(760, 305);
            this.dataGridView1.TabIndex = 1;
            // 
            // predstavaIdDataGridViewTextBoxColumn
            // 
            this.predstavaIdDataGridViewTextBoxColumn.DataPropertyName = "predstavaId";
            this.predstavaIdDataGridViewTextBoxColumn.HeaderText = "predstavaId";
            this.predstavaIdDataGridViewTextBoxColumn.Name = "predstavaIdDataGridViewTextBoxColumn";
            this.predstavaIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.predstavaIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // filmDataGridViewTextBoxColumn
            // 
            this.filmDataGridViewTextBoxColumn.DataPropertyName = "Film";
            this.filmDataGridViewTextBoxColumn.HeaderText = "Film";
            this.filmDataGridViewTextBoxColumn.Name = "filmDataGridViewTextBoxColumn";
            this.filmDataGridViewTextBoxColumn.ReadOnly = true;
            this.filmDataGridViewTextBoxColumn.Width = 200;
            // 
            // datumDataGridViewTextBoxColumn
            // 
            this.datumDataGridViewTextBoxColumn.DataPropertyName = "datum";
            this.datumDataGridViewTextBoxColumn.HeaderText = "Datum";
            this.datumDataGridViewTextBoxColumn.Name = "datumDataGridViewTextBoxColumn";
            this.datumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vrijemeDataGridViewTextBoxColumn
            // 
            this.vrijemeDataGridViewTextBoxColumn.DataPropertyName = "vrijeme";
            this.vrijemeDataGridViewTextBoxColumn.HeaderText = "Vrijeme";
            this.vrijemeDataGridViewTextBoxColumn.Name = "vrijemeDataGridViewTextBoxColumn";
            this.vrijemeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dvoranaDataGridViewTextBoxColumn
            // 
            this.dvoranaDataGridViewTextBoxColumn.DataPropertyName = "Dvorana";
            this.dvoranaDataGridViewTextBoxColumn.HeaderText = "Dvorana";
            this.dvoranaDataGridViewTextBoxColumn.Name = "dvoranaDataGridViewTextBoxColumn";
            this.dvoranaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tipPredstaveIdDataGridViewTextBoxColumn
            // 
            this.tipPredstaveIdDataGridViewTextBoxColumn.DataPropertyName = "tipPredstaveId";
            this.tipPredstaveIdDataGridViewTextBoxColumn.HeaderText = "tipPredstaveId";
            this.tipPredstaveIdDataGridViewTextBoxColumn.Name = "tipPredstaveIdDataGridViewTextBoxColumn";
            this.tipPredstaveIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.tipPredstaveIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // dvoranaIdDataGridViewTextBoxColumn
            // 
            this.dvoranaIdDataGridViewTextBoxColumn.DataPropertyName = "dvoranaId";
            this.dvoranaIdDataGridViewTextBoxColumn.HeaderText = "dvoranaId";
            this.dvoranaIdDataGridViewTextBoxColumn.Name = "dvoranaIdDataGridViewTextBoxColumn";
            this.dvoranaIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.dvoranaIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // filmIdDataGridViewTextBoxColumn
            // 
            this.filmIdDataGridViewTextBoxColumn.DataPropertyName = "filmId";
            this.filmIdDataGridViewTextBoxColumn.HeaderText = "filmId";
            this.filmIdDataGridViewTextBoxColumn.Name = "filmIdDataGridViewTextBoxColumn";
            this.filmIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.filmIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // zaposlenikIdDataGridViewTextBoxColumn
            // 
            this.zaposlenikIdDataGridViewTextBoxColumn.DataPropertyName = "zaposlenikId";
            this.zaposlenikIdDataGridViewTextBoxColumn.HeaderText = "zaposlenikId";
            this.zaposlenikIdDataGridViewTextBoxColumn.Name = "zaposlenikIdDataGridViewTextBoxColumn";
            this.zaposlenikIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.zaposlenikIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // tipPredstaveDataGridViewTextBoxColumn
            // 
            this.tipPredstaveDataGridViewTextBoxColumn.DataPropertyName = "TipPredstave";
            this.tipPredstaveDataGridViewTextBoxColumn.HeaderText = "TipPredstave";
            this.tipPredstaveDataGridViewTextBoxColumn.Name = "tipPredstaveDataGridViewTextBoxColumn";
            this.tipPredstaveDataGridViewTextBoxColumn.ReadOnly = true;
            this.tipPredstaveDataGridViewTextBoxColumn.Visible = false;
            // 
            // zaposlenikDataGridViewTextBoxColumn
            // 
            this.zaposlenikDataGridViewTextBoxColumn.DataPropertyName = "Zaposlenik";
            this.zaposlenikDataGridViewTextBoxColumn.HeaderText = "Zaposlenik";
            this.zaposlenikDataGridViewTextBoxColumn.Name = "zaposlenikDataGridViewTextBoxColumn";
            this.zaposlenikDataGridViewTextBoxColumn.ReadOnly = true;
            this.zaposlenikDataGridViewTextBoxColumn.Visible = false;
            // 
            // rezervacijeDataGridViewTextBoxColumn
            // 
            this.rezervacijeDataGridViewTextBoxColumn.DataPropertyName = "Rezervacije";
            this.rezervacijeDataGridViewTextBoxColumn.HeaderText = "Rezervacije";
            this.rezervacijeDataGridViewTextBoxColumn.Name = "rezervacijeDataGridViewTextBoxColumn";
            this.rezervacijeDataGridViewTextBoxColumn.ReadOnly = true;
            this.rezervacijeDataGridViewTextBoxColumn.Visible = false;
            // 
            // predstavaBindingSource
            // 
            this.predstavaBindingSource.DataSource = typeof(KiNO2.Predstava);
            // 
            // btnNovaPredstava
            // 
            this.btnNovaPredstava.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnNovaPredstava.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNovaPredstava.FlatAppearance.BorderSize = 0;
            this.btnNovaPredstava.FlatAppearance.MouseOverBackColor = System.Drawing.Color.OrangeRed;
            this.btnNovaPredstava.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNovaPredstava.Location = new System.Drawing.Point(23, 247);
            this.btnNovaPredstava.Name = "btnNovaPredstava";
            this.btnNovaPredstava.Size = new System.Drawing.Size(147, 37);
            this.btnNovaPredstava.TabIndex = 0;
            this.btnNovaPredstava.Text = "Nova predstava";
            this.btnNovaPredstava.UseVisualStyleBackColor = false;
            this.btnNovaPredstava.Click += new System.EventHandler(this.btnNovaPredstava_Click);
            // 
            // tabRezervacije
            // 
            this.tabRezervacije.Controls.Add(this.pnlTheaterRoomReservations);
            this.tabRezervacije.Controls.Add(this.pnlReservations);
            this.tabRezervacije.Controls.Add(this.panel2);
            this.tabRezervacije.Location = new System.Drawing.Point(4, 29);
            this.tabRezervacije.Name = "tabRezervacije";
            this.tabRezervacije.Padding = new System.Windows.Forms.Padding(3);
            this.tabRezervacije.Size = new System.Drawing.Size(962, 572);
            this.tabRezervacije.TabIndex = 3;
            this.tabRezervacije.Text = "Rezervacije";
            this.tabRezervacije.UseVisualStyleBackColor = true;
            // 
            // pnlTheaterRoomReservations
            // 
            this.pnlTheaterRoomReservations.BackColor = System.Drawing.Color.IndianRed;
            this.pnlTheaterRoomReservations.BackgroundImage = global::KiNO2.Properties.Resources.screen;
            this.pnlTheaterRoomReservations.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlTheaterRoomReservations.Controls.Add(this.pnlTheaterRoomEditorReservations);
            this.pnlTheaterRoomReservations.Location = new System.Drawing.Point(371, 40);
            this.pnlTheaterRoomReservations.Name = "pnlTheaterRoomReservations";
            this.pnlTheaterRoomReservations.Size = new System.Drawing.Size(452, 457);
            this.pnlTheaterRoomReservations.TabIndex = 9;
            // 
            // pnlTheaterRoomEditorReservations
            // 
            this.pnlTheaterRoomEditorReservations.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlTheaterRoomEditorReservations.BackColor = System.Drawing.Color.Transparent;
            this.pnlTheaterRoomEditorReservations.Location = new System.Drawing.Point(138, 160);
            this.pnlTheaterRoomEditorReservations.Name = "pnlTheaterRoomEditorReservations";
            this.pnlTheaterRoomEditorReservations.Size = new System.Drawing.Size(172, 159);
            this.pnlTheaterRoomEditorReservations.TabIndex = 0;
            // 
            // pnlReservations
            // 
            this.pnlReservations.Controls.Add(this.lblNumberOfTickets);
            this.pnlReservations.Controls.Add(this.nudNumberOfTickets);
            this.pnlReservations.Controls.Add(this.lblTelephone);
            this.pnlReservations.Controls.Add(this.tbxTelephone);
            this.pnlReservations.Controls.Add(this.lblSurename);
            this.pnlReservations.Controls.Add(this.tbxSurename);
            this.pnlReservations.Controls.Add(this.lblName);
            this.pnlReservations.Controls.Add(this.tbxName);
            this.pnlReservations.Controls.Add(this.cbxTiming);
            this.pnlReservations.Controls.Add(this.lblTiming);
            this.pnlReservations.Controls.Add(this.cbxMovies);
            this.pnlReservations.Controls.Add(this.lblMovies);
            this.pnlReservations.Controls.Add(this.calReservations);
            this.pnlReservations.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlReservations.Location = new System.Drawing.Point(3, 3);
            this.pnlReservations.Name = "pnlReservations";
            this.pnlReservations.Size = new System.Drawing.Size(228, 534);
            this.pnlReservations.TabIndex = 3;
            // 
            // lblNumberOfTickets
            // 
            this.lblNumberOfTickets.AutoSize = true;
            this.lblNumberOfTickets.Location = new System.Drawing.Point(56, 486);
            this.lblNumberOfTickets.Name = "lblNumberOfTickets";
            this.lblNumberOfTickets.Size = new System.Drawing.Size(84, 20);
            this.lblNumberOfTickets.TabIndex = 12;
            this.lblNumberOfTickets.Text = "Broj karata:";
            // 
            // nudNumberOfTickets
            // 
            this.nudNumberOfTickets.Location = new System.Drawing.Point(146, 484);
            this.nudNumberOfTickets.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.nudNumberOfTickets.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNumberOfTickets.Name = "nudNumberOfTickets";
            this.nudNumberOfTickets.Size = new System.Drawing.Size(67, 27);
            this.nudNumberOfTickets.TabIndex = 11;
            this.nudNumberOfTickets.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNumberOfTickets.ValueChanged += new System.EventHandler(this.nudNumberOfTickets_ValueChanged);
            // 
            // lblTelephone
            // 
            this.lblTelephone.AutoSize = true;
            this.lblTelephone.Location = new System.Drawing.Point(6, 412);
            this.lblTelephone.Name = "lblTelephone";
            this.lblTelephone.Size = new System.Drawing.Size(98, 20);
            this.lblTelephone.TabIndex = 10;
            this.lblTelephone.Text = "Broj telefona:";
            // 
            // tbxTelephone
            // 
            this.tbxTelephone.Location = new System.Drawing.Point(10, 435);
            this.tbxTelephone.Name = "tbxTelephone";
            this.tbxTelephone.Size = new System.Drawing.Size(203, 27);
            this.tbxTelephone.TabIndex = 9;
            // 
            // lblSurename
            // 
            this.lblSurename.AutoSize = true;
            this.lblSurename.Location = new System.Drawing.Point(6, 352);
            this.lblSurename.Name = "lblSurename";
            this.lblSurename.Size = new System.Drawing.Size(65, 20);
            this.lblSurename.TabIndex = 8;
            this.lblSurename.Text = "Prezime:";
            // 
            // tbxSurename
            // 
            this.tbxSurename.Location = new System.Drawing.Point(10, 375);
            this.tbxSurename.Name = "tbxSurename";
            this.tbxSurename.Size = new System.Drawing.Size(203, 27);
            this.tbxSurename.TabIndex = 7;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(6, 293);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(37, 20);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Ime:";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(10, 316);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(203, 27);
            this.tbxName.TabIndex = 5;
            // 
            // cbxTiming
            // 
            this.cbxTiming.FormattingEnabled = true;
            this.cbxTiming.Location = new System.Drawing.Point(10, 255);
            this.cbxTiming.Name = "cbxTiming";
            this.cbxTiming.Size = new System.Drawing.Size(203, 28);
            this.cbxTiming.TabIndex = 4;
            this.cbxTiming.SelectedIndexChanged += new System.EventHandler(this.cbxTiming_SelectedIndexChanged);
            // 
            // lblTiming
            // 
            this.lblTiming.AutoSize = true;
            this.lblTiming.Location = new System.Drawing.Point(6, 232);
            this.lblTiming.Name = "lblTiming";
            this.lblTiming.Size = new System.Drawing.Size(57, 20);
            this.lblTiming.TabIndex = 3;
            this.lblTiming.Text = "Termin:";
            // 
            // cbxMovies
            // 
            this.cbxMovies.FormattingEnabled = true;
            this.cbxMovies.Location = new System.Drawing.Point(10, 194);
            this.cbxMovies.Name = "cbxMovies";
            this.cbxMovies.Size = new System.Drawing.Size(203, 28);
            this.cbxMovies.TabIndex = 2;
            this.cbxMovies.SelectedIndexChanged += new System.EventHandler(this.cbxMovies_SelectedIndexChanged);
            // 
            // lblMovies
            // 
            this.lblMovies.AutoSize = true;
            this.lblMovies.Location = new System.Drawing.Point(6, 171);
            this.lblMovies.Name = "lblMovies";
            this.lblMovies.Size = new System.Drawing.Size(40, 20);
            this.lblMovies.TabIndex = 1;
            this.lblMovies.Text = "Film:";
            // 
            // calReservations
            // 
            this.calReservations.Dock = System.Windows.Forms.DockStyle.Top;
            this.calReservations.Location = new System.Drawing.Point(0, 0);
            this.calReservations.Name = "calReservations";
            this.calReservations.TabIndex = 0;
            this.calReservations.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calReservations_DateChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.btnBuyTickets);
            this.panel2.Controls.Add(this.btnReservationsList);
            this.panel2.Controls.Add(this.btnMakeReservation);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 537);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(956, 32);
            this.panel2.TabIndex = 2;
            // 
            // btnBuyTickets
            // 
            this.btnBuyTickets.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBuyTickets.Location = new System.Drawing.Point(728, 0);
            this.btnBuyTickets.Name = "btnBuyTickets";
            this.btnBuyTickets.Size = new System.Drawing.Size(114, 32);
            this.btnBuyTickets.TabIndex = 4;
            this.btnBuyTickets.Text = "Kupi karte";
            this.btnBuyTickets.UseVisualStyleBackColor = true;
            this.btnBuyTickets.Click += new System.EventHandler(this.btnBuyTickets_Click);
            // 
            // btnReservationsList
            // 
            this.btnReservationsList.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnReservationsList.Location = new System.Drawing.Point(0, 0);
            this.btnReservationsList.Name = "btnReservationsList";
            this.btnReservationsList.Size = new System.Drawing.Size(187, 32);
            this.btnReservationsList.TabIndex = 3;
            this.btnReservationsList.Text = "Storniranje rezervacije";
            this.btnReservationsList.UseVisualStyleBackColor = true;
            this.btnReservationsList.Click += new System.EventHandler(this.btnReservationsList_Click);
            // 
            // btnMakeReservation
            // 
            this.btnMakeReservation.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnMakeReservation.Location = new System.Drawing.Point(842, 0);
            this.btnMakeReservation.Name = "btnMakeReservation";
            this.btnMakeReservation.Size = new System.Drawing.Size(114, 32);
            this.btnMakeReservation.TabIndex = 2;
            this.btnMakeReservation.Text = "Rezerviraj";
            this.btnMakeReservation.UseVisualStyleBackColor = true;
            this.btnMakeReservation.Click += new System.EventHandler(this.btnMakeReservation_Click);
            // 
            // tabOsoblje
            // 
            this.tabOsoblje.Controls.Add(this.lblZaposlenici);
            this.tabOsoblje.Controls.Add(this.lblKategorijeZaposlenika);
            this.tabOsoblje.Controls.Add(this.btnUkloniKorisnika);
            this.tabOsoblje.Controls.Add(this.btnAzuriraj);
            this.tabOsoblje.Controls.Add(this.dgvKategorijaZaposlenika);
            this.tabOsoblje.Controls.Add(this.dgvZaposlenici);
            this.tabOsoblje.Controls.Add(this.btnDodajKorisnika);
            this.tabOsoblje.Location = new System.Drawing.Point(4, 29);
            this.tabOsoblje.Name = "tabOsoblje";
            this.tabOsoblje.Padding = new System.Windows.Forms.Padding(3);
            this.tabOsoblje.Size = new System.Drawing.Size(962, 572);
            this.tabOsoblje.TabIndex = 5;
            this.tabOsoblje.Text = "Osoblje";
            this.tabOsoblje.UseVisualStyleBackColor = true;
            // 
            // lblZaposlenici
            // 
            this.lblZaposlenici.AutoSize = true;
            this.lblZaposlenici.Location = new System.Drawing.Point(8, 193);
            this.lblZaposlenici.Name = "lblZaposlenici";
            this.lblZaposlenici.Size = new System.Drawing.Size(88, 20);
            this.lblZaposlenici.TabIndex = 9;
            this.lblZaposlenici.Text = "Zaposlenici:";
            // 
            // lblKategorijeZaposlenika
            // 
            this.lblKategorijeZaposlenika.AutoSize = true;
            this.lblKategorijeZaposlenika.Location = new System.Drawing.Point(8, 7);
            this.lblKategorijeZaposlenika.Name = "lblKategorijeZaposlenika";
            this.lblKategorijeZaposlenika.Size = new System.Drawing.Size(165, 20);
            this.lblKategorijeZaposlenika.TabIndex = 8;
            this.lblKategorijeZaposlenika.Text = "Kategorija Zaposlenika:";
            // 
            // btnUkloniKorisnika
            // 
            this.btnUkloniKorisnika.Location = new System.Drawing.Point(501, 529);
            this.btnUkloniKorisnika.Name = "btnUkloniKorisnika";
            this.btnUkloniKorisnika.Size = new System.Drawing.Size(147, 37);
            this.btnUkloniKorisnika.TabIndex = 7;
            this.btnUkloniKorisnika.Text = "Ukloni korisnika";
            this.btnUkloniKorisnika.UseVisualStyleBackColor = true;
            this.btnUkloniKorisnika.Click += new System.EventHandler(this.btnUkloniKorisnika_Click);
            // 
            // btnAzuriraj
            // 
            this.btnAzuriraj.AutoSize = true;
            this.btnAzuriraj.Location = new System.Drawing.Point(654, 529);
            this.btnAzuriraj.Name = "btnAzuriraj";
            this.btnAzuriraj.Size = new System.Drawing.Size(147, 37);
            this.btnAzuriraj.TabIndex = 6;
            this.btnAzuriraj.Text = "Ažuriraj korisnika";
            this.btnAzuriraj.UseVisualStyleBackColor = true;
            this.btnAzuriraj.Click += new System.EventHandler(this.btnAzuriraj_Click);
            // 
            // dgvKategorijaZaposlenika
            // 
            this.dgvKategorijaZaposlenika.AllowUserToAddRows = false;
            this.dgvKategorijaZaposlenika.AllowUserToDeleteRows = false;
            this.dgvKategorijaZaposlenika.AutoGenerateColumns = false;
            this.dgvKategorijaZaposlenika.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvKategorijaZaposlenika.BackgroundColor = System.Drawing.Color.White;
            this.dgvKategorijaZaposlenika.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKategorijaZaposlenika.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10});
            this.dgvKategorijaZaposlenika.DataSource = this.kategorijaZaposlenikaBindingSource;
            this.dgvKategorijaZaposlenika.GridColor = System.Drawing.Color.WhiteSmoke;
            this.dgvKategorijaZaposlenika.Location = new System.Drawing.Point(8, 30);
            this.dgvKategorijaZaposlenika.Name = "dgvKategorijaZaposlenika";
            this.dgvKategorijaZaposlenika.Size = new System.Drawing.Size(951, 150);
            this.dgvKategorijaZaposlenika.TabIndex = 5;
            this.dgvKategorijaZaposlenika.SelectionChanged += new System.EventHandler(this.dgvKategorijaZaposlenika_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "kategorijaZaposlenikaId";
            this.dataGridViewTextBoxColumn8.HeaderText = "kategorijaZaposlenikaId";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            this.dataGridViewTextBoxColumn8.Width = 145;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "naziv";
            this.dataGridViewTextBoxColumn9.HeaderText = "naziv";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 68;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "opis";
            this.dataGridViewTextBoxColumn10.HeaderText = "opis";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 62;
            // 
            // kategorijaZaposlenikaBindingSource
            // 
            this.kategorijaZaposlenikaBindingSource.DataMember = "KategorijaZaposlenika";
            this.kategorijaZaposlenikaBindingSource.DataSource = this._16058_DBDataSet;
            // 
            // _16058_DBDataSet
            // 
            this._16058_DBDataSet.DataSetName = "_16058_DBDataSet";
            this._16058_DBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dgvZaposlenici
            // 
            this.dgvZaposlenici.AllowUserToAddRows = false;
            this.dgvZaposlenici.AllowUserToDeleteRows = false;
            this.dgvZaposlenici.AutoGenerateColumns = false;
            this.dgvZaposlenici.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.OrangeRed;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvZaposlenici.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvZaposlenici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvZaposlenici.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.dgvZaposlenici.DataSource = this.zaposlenikBindingSource1;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.OrangeRed;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvZaposlenici.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvZaposlenici.Location = new System.Drawing.Point(8, 216);
            this.dgvZaposlenici.Name = "dgvZaposlenici";
            this.dgvZaposlenici.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvZaposlenici.Size = new System.Drawing.Size(951, 307);
            this.dgvZaposlenici.TabIndex = 4;
            this.dgvZaposlenici.SelectionChanged += new System.EventHandler(this.dgvZaposlenici_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "zaposlenikId";
            this.dataGridViewTextBoxColumn1.HeaderText = "zaposlenikId";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ime";
            this.dataGridViewTextBoxColumn2.HeaderText = "ime";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "prezime";
            this.dataGridViewTextBoxColumn3.HeaderText = "prezime";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "email";
            this.dataGridViewTextBoxColumn4.HeaderText = "email";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "lozinka";
            this.dataGridViewTextBoxColumn5.HeaderText = "lozinka";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "telefon";
            this.dataGridViewTextBoxColumn6.HeaderText = "telefon";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "kategorijaZaposlenikaId";
            this.dataGridViewTextBoxColumn7.HeaderText = "kategorijaZaposlenikaId";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // zaposlenikBindingSource1
            // 
            this.zaposlenikBindingSource1.DataSource = typeof(KiNO2.Zaposlenik);
            // 
            // btnDodajKorisnika
            // 
            this.btnDodajKorisnika.AutoSize = true;
            this.btnDodajKorisnika.Location = new System.Drawing.Point(807, 529);
            this.btnDodajKorisnika.Name = "btnDodajKorisnika";
            this.btnDodajKorisnika.Size = new System.Drawing.Size(147, 37);
            this.btnDodajKorisnika.TabIndex = 3;
            this.btnDodajKorisnika.Text = "Dodaj korisnika";
            this.btnDodajKorisnika.UseVisualStyleBackColor = true;
            this.btnDodajKorisnika.Click += new System.EventHandler(this.btnDodajKorisnika_Click);
            // 
            // tabKarte
            // 
            this.tabKarte.Controls.Add(this.lblBrojRezervacije);
            this.tabKarte.Controls.Add(this.txtBrojRezervacije);
            this.tabKarte.Controls.Add(this.btnKupi);
            this.tabKarte.Controls.Add(this.listBox1);
            this.tabKarte.Controls.Add(this.lblNaruciteljPrezime);
            this.tabKarte.Controls.Add(this.lblNaruciteljIme);
            this.tabKarte.Controls.Add(this.txtNaruciteljPrezime);
            this.tabKarte.Controls.Add(this.txtNaruciteljIme);
            this.tabKarte.Controls.Add(this.btnTrazi);
            this.tabKarte.Location = new System.Drawing.Point(4, 29);
            this.tabKarte.Name = "tabKarte";
            this.tabKarte.Padding = new System.Windows.Forms.Padding(3);
            this.tabKarte.Size = new System.Drawing.Size(962, 572);
            this.tabKarte.TabIndex = 4;
            this.tabKarte.Text = "Karte";
            this.tabKarte.UseVisualStyleBackColor = true;
            // 
            // lblBrojRezervacije
            // 
            this.lblBrojRezervacije.AutoSize = true;
            this.lblBrojRezervacije.Location = new System.Drawing.Point(398, 18);
            this.lblBrojRezervacije.Name = "lblBrojRezervacije";
            this.lblBrojRezervacije.Size = new System.Drawing.Size(118, 20);
            this.lblBrojRezervacije.TabIndex = 10;
            this.lblBrojRezervacije.Text = "Broj Rezervacije:";
            // 
            // txtBrojRezervacije
            // 
            this.txtBrojRezervacije.Location = new System.Drawing.Point(522, 15);
            this.txtBrojRezervacije.Name = "txtBrojRezervacije";
            this.txtBrojRezervacije.Size = new System.Drawing.Size(100, 27);
            this.txtBrojRezervacije.TabIndex = 9;
            // 
            // btnKupi
            // 
            this.btnKupi.Location = new System.Drawing.Point(806, 17);
            this.btnKupi.Name = "btnKupi";
            this.btnKupi.Size = new System.Drawing.Size(137, 28);
            this.btnKupi.TabIndex = 8;
            this.btnKupi.Text = "Kupi";
            this.btnKupi.UseVisualStyleBackColor = true;
            this.btnKupi.Click += new System.EventHandler(this.btnKupi_Click);
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(3, 65);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(956, 504);
            this.listBox1.TabIndex = 7;
            // 
            // lblNaruciteljPrezime
            // 
            this.lblNaruciteljPrezime.AutoSize = true;
            this.lblNaruciteljPrezime.Location = new System.Drawing.Point(204, 18);
            this.lblNaruciteljPrezime.Name = "lblNaruciteljPrezime";
            this.lblNaruciteljPrezime.Size = new System.Drawing.Size(65, 20);
            this.lblNaruciteljPrezime.TabIndex = 6;
            this.lblNaruciteljPrezime.Text = "Prezime:";
            // 
            // lblNaruciteljIme
            // 
            this.lblNaruciteljIme.AutoSize = true;
            this.lblNaruciteljIme.Location = new System.Drawing.Point(16, 18);
            this.lblNaruciteljIme.Name = "lblNaruciteljIme";
            this.lblNaruciteljIme.Size = new System.Drawing.Size(37, 20);
            this.lblNaruciteljIme.TabIndex = 5;
            this.lblNaruciteljIme.Text = "Ime:";
            // 
            // txtNaruciteljPrezime
            // 
            this.txtNaruciteljPrezime.Location = new System.Drawing.Point(275, 15);
            this.txtNaruciteljPrezime.Name = "txtNaruciteljPrezime";
            this.txtNaruciteljPrezime.Size = new System.Drawing.Size(100, 27);
            this.txtNaruciteljPrezime.TabIndex = 4;
            // 
            // txtNaruciteljIme
            // 
            this.txtNaruciteljIme.Location = new System.Drawing.Point(72, 15);
            this.txtNaruciteljIme.Name = "txtNaruciteljIme";
            this.txtNaruciteljIme.Size = new System.Drawing.Size(100, 27);
            this.txtNaruciteljIme.TabIndex = 3;
            // 
            // btnTrazi
            // 
            this.btnTrazi.Location = new System.Drawing.Point(652, 15);
            this.btnTrazi.Name = "btnTrazi";
            this.btnTrazi.Size = new System.Drawing.Size(137, 30);
            this.btnTrazi.TabIndex = 0;
            this.btnTrazi.Text = "Traži";
            this.btnTrazi.UseVisualStyleBackColor = true;
            this.btnTrazi.Click += new System.EventHandler(this.btnTrazi_Click);
            // 
            // tabPomoc
            // 
            this.tabPomoc.Location = new System.Drawing.Point(4, 29);
            this.tabPomoc.Name = "tabPomoc";
            this.tabPomoc.Padding = new System.Windows.Forms.Padding(3);
            this.tabPomoc.Size = new System.Drawing.Size(962, 572);
            this.tabPomoc.TabIndex = 6;
            this.tabPomoc.Text = "Pomoć";
            this.tabPomoc.UseVisualStyleBackColor = true;
            // 
            // zaposlenikTableAdapter
            // 
            this.zaposlenikTableAdapter.ClearBeforeFill = true;
            // 
            // kategorijaZaposlenikaTableAdapter
            // 
            this.kategorijaZaposlenikaTableAdapter.ClearBeforeFill = true;
            // 
            // pnlLoginPanel
            // 
            this.pnlLoginPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlLoginPanel.BackgroundImage")));
            this.pnlLoginPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlLoginPanel.Controls.Add(this.lblPrijava);
            this.pnlLoginPanel.Controls.Add(this.lblLozinka);
            this.pnlLoginPanel.Controls.Add(this.lblMail);
            this.pnlLoginPanel.Controls.Add(this.txtLozinka);
            this.pnlLoginPanel.Controls.Add(this.txtKorisnickoIme);
            this.pnlLoginPanel.Controls.Add(this.btnLogin);
            this.pnlLoginPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLoginPanel.Location = new System.Drawing.Point(0, 0);
            this.pnlLoginPanel.Name = "pnlLoginPanel";
            this.pnlLoginPanel.Size = new System.Drawing.Size(970, 605);
            this.pnlLoginPanel.TabIndex = 1;
            // 
            // lblPrijava
            // 
            this.lblPrijava.AutoSize = true;
            this.lblPrijava.BackColor = System.Drawing.Color.Transparent;
            this.lblPrijava.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPrijava.ForeColor = System.Drawing.Color.White;
            this.lblPrijava.Location = new System.Drawing.Point(400, 406);
            this.lblPrijava.Name = "lblPrijava";
            this.lblPrijava.Size = new System.Drawing.Size(177, 42);
            this.lblPrijava.TabIndex = 5;
            this.lblPrijava.Text = " PRIJAVA";
            // 
            // lblLozinka
            // 
            this.lblLozinka.AutoSize = true;
            this.lblLozinka.BackColor = System.Drawing.Color.Transparent;
            this.lblLozinka.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLozinka.ForeColor = System.Drawing.Color.White;
            this.lblLozinka.Location = new System.Drawing.Point(372, 478);
            this.lblLozinka.Name = "lblLozinka";
            this.lblLozinka.Size = new System.Drawing.Size(69, 16);
            this.lblLozinka.TabIndex = 4;
            this.lblLozinka.Text = "Lozinka: ";
            // 
            // lblMail
            // 
            this.lblMail.AutoSize = true;
            this.lblMail.BackColor = System.Drawing.Color.Transparent;
            this.lblMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblMail.ForeColor = System.Drawing.Color.White;
            this.lblMail.Location = new System.Drawing.Point(381, 452);
            this.lblMail.Name = "lblMail";
            this.lblMail.Size = new System.Drawing.Size(60, 16);
            this.lblMail.TabIndex = 3;
            this.lblMail.Text = "E-mail: ";
            // 
            // txtLozinka
            // 
            this.txtLozinka.Location = new System.Drawing.Point(447, 477);
            this.txtLozinka.Name = "txtLozinka";
            this.txtLozinka.Size = new System.Drawing.Size(100, 20);
            this.txtLozinka.TabIndex = 2;
            this.txtLozinka.UseSystemPasswordChar = true;
            // 
            // txtKorisnickoIme
            // 
            this.txtKorisnickoIme.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtKorisnickoIme.Location = new System.Drawing.Point(447, 451);
            this.txtKorisnickoIme.Multiline = true;
            this.txtKorisnickoIme.Name = "txtKorisnickoIme";
            this.txtKorisnickoIme.Size = new System.Drawing.Size(100, 20);
            this.txtKorisnickoIme.TabIndex = 1;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.White;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnLogin.Location = new System.Drawing.Point(457, 503);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Prijava";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // frmGlavnaForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 605);
            this.Controls.Add(this.pnlLoginPanel);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmGlavnaForma";
            this.Text = "KiNO2";
            this.Load += new System.EventHandler(this.frmGlavnaForma_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmGlavnaForma_KeyDown);
            this.Resize += new System.EventHandler(this.frmGlavnaForma_Resize);
            this.tabControl.ResumeLayout(false);
            this.tabFilmovi.ResumeLayout(false);
            this.pnlMainMoviePanel.ResumeLayout(false);
            this.tabDvorane.ResumeLayout(false);
            this.pnlTheaterRoom.ResumeLayout(false);
            this.pnlBottomPanelTheaterRooms.ResumeLayout(false);
            this.tabPredstave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.predstavaBindingSource)).EndInit();
            this.tabRezervacije.ResumeLayout(false);
            this.pnlTheaterRoomReservations.ResumeLayout(false);
            this.pnlReservations.ResumeLayout(false);
            this.pnlReservations.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfTickets)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tabOsoblje.ResumeLayout(false);
            this.tabOsoblje.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKategorijaZaposlenika)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kategorijaZaposlenikaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._16058_DBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvZaposlenici)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zaposlenikBindingSource1)).EndInit();
            this.tabKarte.ResumeLayout(false);
            this.tabKarte.PerformLayout();
            this.pnlLoginPanel.ResumeLayout(false);
            this.pnlLoginPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabFilmovi;
        private System.Windows.Forms.TabPage tabDvorane;
        private System.Windows.Forms.TabPage tabPredstave;
        private System.Windows.Forms.TabPage tabRezervacije;
        private System.Windows.Forms.TabPage tabKarte;
        private System.Windows.Forms.TabPage tabOsoblje;
        private System.Windows.Forms.Panel pnlLoginPanel;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtLozinka;
        private System.Windows.Forms.TextBox txtKorisnickoIme;
        private System.Windows.Forms.Label lblPrijava;
        private System.Windows.Forms.Label lblLozinka;
        private System.Windows.Forms.Label lblMail;
        private System.Windows.Forms.Panel pnlBottomPanelTheaterRooms;
        private System.Windows.Forms.Button btnAddNewTheatherRoom;
        private System.Windows.Forms.ListBox lbxTheaterRooms;
        private System.Windows.Forms.Button btnEditTheaterRoom;
        private System.Windows.Forms.Panel pnlTheaterRoom;
        private System.Windows.Forms.Panel pnlTheaterRoomEditor;
        private System.Windows.Forms.Button btnDeleteTheaterRoom;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnMakeReservation;
        private System.Windows.Forms.Panel pnlReservations;
        private System.Windows.Forms.ComboBox cbxTiming;
        private System.Windows.Forms.Label lblTiming;
        private System.Windows.Forms.ComboBox cbxMovies;
        private System.Windows.Forms.Label lblMovies;
        private System.Windows.Forms.MonthCalendar calReservations;
        private System.Windows.Forms.Button btnReservationsList;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbxName;
        private System.Windows.Forms.Label lblSurename;
        private System.Windows.Forms.TextBox tbxSurename;
        private System.Windows.Forms.Panel pnlTheaterRoomReservations;
        private System.Windows.Forms.Panel pnlTheaterRoomEditorReservations;
        private System.Windows.Forms.Label lblNumberOfTickets;
        private System.Windows.Forms.NumericUpDown nudNumberOfTickets;
        private System.Windows.Forms.Label lblTelephone;
        private System.Windows.Forms.TextBox tbxTelephone;
        private System.Windows.Forms.Button btnBuyTickets;
        private System.Windows.Forms.Button btnDodajNoviFilm;
        private System.Windows.Forms.Button btnUkloniFilmIzBaze;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button btnDodajKorisnika;
        private System.Windows.Forms.DataGridView dgvZaposlenici;
        //izmjena
        //private System.Windows.Forms.DataGridViewTextBoxColumn zaposlenikIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn imeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prezimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lozinkaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kategorijaZaposlenikaIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dgvKategorijaZaposlenika;
        private System.Windows.Forms.DataGridViewTextBoxColumn kategorijaZaposlenikaIdDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazivDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn opisDataGridViewTextBoxColumn;
        private _16058_DBDataSetTableAdapters.ZaposlenikTableAdapter zaposlenikTableAdapter;
        private System.Windows.Forms.BindingSource kategorijaZaposlenikaBindingSource;
        private _16058_DBDataSetTableAdapters.KategorijaZaposlenikaTableAdapter kategorijaZaposlenikaTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.Button btnAzuriraj;
        private System.Windows.Forms.BindingSource zaposlenikBindingSource1;
        private _16058_DBDataSet _16058_DBDataSet;
        private System.Windows.Forms.Button btnUkloniKorisnika;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.Label lblZaposlenici;
        private System.Windows.Forms.Label lblKategorijeZaposlenika;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnNovaPredstava;
        private System.Windows.Forms.DateTimePicker predDatumVrijeme;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource predstavaBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn predstavaIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn filmDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vrijemeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvoranaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipPredstaveIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvoranaIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn filmIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zaposlenikIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipPredstaveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zaposlenikDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rezervacijeDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnUkloniPredstavu;
        private System.Windows.Forms.Panel pnlMainMoviePanel;
        private View.MovieTableLayoutPanel movieTableLayoutPanel1;
        private System.Windows.Forms.Button btnTrazi;
        private System.Windows.Forms.TextBox txtNaruciteljPrezime;
        private System.Windows.Forms.TextBox txtNaruciteljIme;
        private Label lblNaruciteljIme;
        private Label lblNaruciteljPrezime;
        private ListBox listBox1;
        private Button btnKupi;
        private Label lblBrojRezervacije;
        private TextBox txtBrojRezervacije;
        private TabPage tabPomoc;
    }
}

