﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiNODLL
{
    public class Crypto
    {
        /// <summary>
        /// Metoda za kreiranje sha256 Hasha
        /// </summary>
        /// <param name="lozinka"></param>
        /// <returns></returns>
        public static string sha256(string lozinka)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(lozinka), 0, Encoding.UTF8.GetByteCount(lozinka));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}
